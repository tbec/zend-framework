GIT repository:  https://tbec@bitbucket.org/tbec/zend-framework.git (not updated)

Installation:

git clone https://tbec@bitbucket.org/tbec/zend-framework.git

using composer (be sure that you have installed svn and git)
php composer.phar self-update
php composer.phar install

preparing entities
./vendor/doctrine/doctrine-module/bin/doctrine-module orm:convert-mapping --namespace="Application\\Entity\\" --force  --from-database annotation ./module/Application/src/

./vendor/doctrine/doctrine-module/bin/doctrine-module orm:generate-entities ./module/Application/src/ --generate-annotations=true
 
database
 /httpdocs/data/database/dump.sql
 
 
Frontpage:
![alt tag](https://bitbucket.org/tbec/zend-framework/raw/a350ca65856860636265ec8a7670631ab0e2ba02/frontend.png)
 
 Admin area:
 ![alt tag](https://bitbucket.org/tbec/zend-framework/raw/66b418e3b6ff36aa8f897a48060b7bafef297502/admin.png)
 
 Responsive admin:
  ![alt tag](https://bitbucket.org/tbec/zend-framework/raw/a350ca65856860636265ec8a7670631ab0e2ba02/responsive.png)

var UserApi = function(){
    this.settings = {}
	
	this.loader = function(opt)
	{
		if(opt) $('.loader').show();
		else $('.loader').hide();
	}
	
    this.init = function(){
        if($('.user-register-form').length)
        {
            var l = $('.user-register-form');
            l.validity(function(){
                var l = $('.user-register-form');
                $("#login",l).require();
                $("#email",l).require();
            });

            l.ajaxForm({
                beforeSubmit:function(){
					notification.clear()
                },
                success:function(d){
                    d = $.parseJSON(d);
                    if(d) {
                      notification.show(d,'.user-register-form ')
                    }
                }
            })
        }
        
        if($('.user-login-form').length)
        {
            var l = $('.user-login-form');
            l.validity(function(){
                var l = $('.user-login-form');
                $("#login",l).require();
                $("#password",l).require();
            });

            l.ajaxForm({
                beforeSubmit:function(){
					user.loader(1);
					notification.clear()
                },
                success:function(d){
					user.loader(0);
                    d = $.parseJSON(d);
                    if(d) {
                      
					  if(d.type==1) window.location.href=d.redirect;
					  else alert(d.msg);
                    }
                }
            })
        }
    }
}

var user = new UserApi();
$(function($){
    user.init();
});
var NotificationApi = function(){
    this.settings = {}
	
	this.clear = function()
	{
		$('.notify').fadeOut()
	}
	
    this.show = function(d,form){
		if(d.errors)
		{
			$.each(d.errors,function(k,v){
			if (!$(form+'#e_'+k).length) $(form+'#'+k).parent().append('<div class="validity-tooltip" id="e_'+k+'" style="right:0px; bottom:0px">'+v+'<div class="validity-tooltip-outer"><div class="validity-tooltip-inner"></div></div></div>')
			$(form+'#'+k).focus(function(){ $(form+'#e_'+k).hide() })
			
			})
		}
	
        $('#notifications').html('<div style="display:none" class="notify '+((d.type)?'success':'error')+'box"> <h1>'+d.title+'</h1> <p>'+d.msg+'</p> </div>');
		$('.notify').fadeIn()
    }
}

var notification = new NotificationApi();
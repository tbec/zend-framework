var CartApi = function(){
    this.settings = {}
	
	this.popup = function(xc_id,obj){
		$('.xc_popup').hide();
		
		var f = ($('#xc_'+xc_id).length)?$('#xc_'+xc_id):$('<form id="xc_'+xc_id+'" class="xc_popup" method="post" action="'+base+'/xcart/add"><input type="hidden" name="xc_id" value="'+xc_id+'" /><input name="qty" class="qty" type="text" value="1" /><input class="submit" type="submit" value="Add to cart" /></form>');
		f.show();
		var xc_allow=0;
		f.ajaxForm({
			beforeSubmit:function(){
				var options =$(obj).hasClass('opt'); 
				if(options) return cart.options(xc_id);
				xc_allow = 0;
				$('#xc_'+xc_id).effect("transfer", { to: $( "#xc_cart" ).eq(0) }, 300,function(){  if(xc_allow) $('.ui-effects-transfer').remove(); xc_allow=1; });
			},
			dataType:'json',
			success:function(d){
				cart.refresh_callback(d);
				if(xc_allow) $('.ui-effects-transfer').remove();
				xc_allow =1;
			}
		})
		
		if(!$('#xc_'+xc_id).length) $(obj).append(f);
		
		
	}
	
	this.options = function(xc_id)
	{
		var opts = '<select name="colour"><option value="red">Red</option><option value="green">Green</option></select>';
		var f = ($('#xc_opts_'+xc_id).length)?$('#xc_opts_'+xc_id):$('<form action="'+base+'/xcart/add" id="xc_opts_'+xc_id+'" class="xc_opts" method="post" action=""><input type="hidden" name="xc_id" value="'+xc_id+'" />'+opts+'<div class="xc_close" onclick="$(this).parent().hide()"></div><input name="qty" class="qty" type="text" value="1" /><input class="submit" type="submit" value="Add to cart" /></form>');
		f.show().css('visibility','hidden');
		
		f.ajaxForm({
			beforeSubmit:function(){
				xc_allow = 0;
				$('#xc_opts_'+xc_id).effect("transfer", { to: $( "#xc_cart" ).eq(0) }, 300,function(){  if(xc_allow) $('.ui-effects-transfer').remove(); xc_allow=1; }).hide();
			},
			dataType:'json',
			success:function(d){

				cart.refresh_callback(d);
				if(xc_allow) $('.ui-effects-transfer').remove();
				xc_allow =1;
			}
		})
		
		if(!$('#xc_opts_'+xc_id).length) $('body').append(f);
		$('#xc_'+xc_id).effect("transfer", { to: $( "#xc_opts_"+xc_id ).eq(0) }, 100,function(){ f.css('visibility','visible'); $('.ui-effects-transfer').remove(); });
		
		return false;
	}
	
	this.update=function(cart_id,quantity){
        $.ajax({
            url:'/xcart/update?cart_id='+cart_id+'&qty='+quantity,
			dataType:'json',
            success: cart.refresh_callback
        })
    
        return false
    }
    
    this.del=function(product_id){
        $.ajax({
            url:'/cart/del?ajax=1',
            success: cart.refresh_callback
        })
    
        return false
    }
	
	this.clear=function(product_id){
        $.ajax({
            url:'/cart/clear?ajax=1',
            success: cart.refresh_callback
        })
    
        return false
    }
	
	this.refresh_callback=function(d){
		if(d.cart) $('#xc_cart .xc_content').html(d.cart);
		if(d.checkout) $('#xc_checkout').html(d.checkout);
	}
	
	this.init = function(){
		jQuery(function($){
			$('.xc').mouseover(function(){
			  var xc_id = $(this).attr('class').split('p_');
			  xc_id = xc_id[1].split(' ');
			  xc_id = parseInt(xc_id[0]);
			
			  cart.popup(xc_id,this);
			})
		
			var form = $('#xc_cart input[type=submit]');
			form.click(function(){
				if (!$('#xc_checkout').length) 
				{
					$('body').append('<form action="'+base+'/xcart/checkout" method="post" id="xc_checkout" class="xc_checkout">loading</form>');
					var f = $('#xc_checkout');
					f.ajaxForm({
						beforeSubmit:function(){
							return false;
						},
						dataType:'json',
						success:function(d){

							cart.refresh_callback(d);
							if(xc_allow) $('.ui-effects-transfer').remove();
							xc_allow =1;
						}
					})
				}
				$('#xc_checkout').show();
				
				$.ajax({
					url:base+'/xcart/checkout',
					dataType:'json',
					success: cart.refresh_callback
				})
				
				return false;
			})
		})
	}
}

var cart = new CartApi();
cart.init();
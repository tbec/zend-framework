var ProjectApi = function(){
    this.settings = {}
	
    this.add = function (d){
        
    }
    
    this.init = function()
    {
        var form = '#project-add-form';
        
        if($(form).length) {
            $('#start,#end',$(form)).mask("99/99/9999");

            $('#end').datepick({ minDate:new Date(),dateFormat:'mm/dd/yyyy',monthsToShow: 2 });

            $('#description').limit('500','#description_limit');
            $('#name').limit('100','#name_limit');
            $('#budget').priceFormat({prefix: '',thousandsSeparator: ''});
            $(form).validity(function(){
                $("#name,#description").require();
                $("#end").match("date").greaterThan(new Date());
                $("#budget").match("usd").greaterThan(0);
            });

            $(form).ajaxForm({
                beforeSubmit:function(){

                },
                success:function(d){
                    d = $.parseJSON(d);
                    if(d.result) {
                        if($('#my-projects').length) ajax.load('/project/get','#my-projects');
                    }
                }
            })
        }
        
        $('#my-projects li').draggable({
         revert: true
         });
    }
}

var project = new ProjectApi();
$(function($){
    project.init();
});
var TeamApi = function(){
    this.settings = {}
	
    this.add = function (d){
        
    }
    
    this.init = function()
    {
        var form = '#team-add-form';
        
        if($(form).length) {
            $('#description').limit('500','#description_limit');
            $('#name').limit('100','#name_limit');
            
            
            $("#source").sortable({  
            connectWith: "#target", distance: 20, stop: function(event, ui){
 event.preventDefault();
 },
            });  
            $("#target").sortable({  
              connectWith: "#source" , distance: 20, stop: function(event, ui){
event.preventDefault();
 },
            }); 
           
            
            $('#target li,#source li').click(function(){
                if($(this).parent().attr('id')=='source') var s ='#target'; else var s ='#source';
                $(this).appendTo($(s));
            })
            
            $(form).validity(function(){
                $("#name,#description").require();
            });

            $(form).ajaxForm({
                beforeSubmit:function(formData, jqForm, options){
                    var mmc = [];
                   $.each($('input',$('#target')),function(k,v){
                       mmc[mmc.length] = $(v).val()
                   });
                   
                   formData.push({ name: 'members', value: mmc });
                },
                success:function(d){
                    d = $.parseJSON(d);
                    if(d.result) {
                        if($('#my-teams').length) ajax.load('/team/get','#my-teams');
                    }
                }
            })
        }
    }
}

var team = new TeamApi();
$(function($){
    team.init();
});
var TaskApi = function(){
    this.settings = {}
	
    this.add = function (d){
        
    }
    
    this.init = function()
    {
        var form = '#task-add-form';
        
        if($(form).length) {
            $('#end',$(form)).mask("99/99/9999");

            $('#end').datepick({ minDate:new Date(),dateFormat:'mm/dd/yyyy',monthsToShow: 2 });

            $('#description').limit('500','#description_limit');
            $('#name').limit('100','#name_limit');
            
            
            $("#source").sortable({  
            connectWith: "#target", distance: 20, stop: function(event, ui){
 event.preventDefault();
 },
            });  
            $("#target").sortable({  
              connectWith: "#source" , distance: 20, stop: function(event, ui){
event.preventDefault();
 },
            }); 
           
            
            $('#target li,#source li').click(function(){
                if($(this).parent().attr('id')=='source') var s ='#target'; else var s ='#source';
                $(this).appendTo($(s));
            })
            
            $(form).validity(function(){
                $("#name,#description").require();
                $("#end").match("date").greaterThan(new Date());
            });

            $(form).ajaxForm({
                beforeSubmit:function(){

                },
                success:function(d){
                    d = $.parseJSON(d);
                    if(d.result) {
                        if($('#my-tasks').length) ajax.load('/task/get','#my-tasks');
                    }
                }
            })
        }
        
        $('#my-tasks li').draggable({
         revert: true
         });
    }
}

var task = new TaskApi();
$(function($){
    task.init();
});
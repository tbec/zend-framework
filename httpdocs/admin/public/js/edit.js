var FormApi = function(){
    this.settings = {}
	
    this.init = function()
    {
               form.settings.active_tab = 'tab_0';
        
               $('.tabs-holder li a').click(function(){
                   $('#edit .form_fields_container li.'+form.settings.active_tab).hide();
                   form.settings.active_tab = $(this).attr('id');
                   $('#edit .form_fields_container li.'+form.settings.active_tab).css('display','inline-block');
                   
                   return false;
               })
               $('.axform').each(function(k,f){
                        
                        form.init_fields(f);
			form.init_validators(f);
                        
			form.settings.form = $(f);
			form.settings.form.ajaxForm({
                beforeSubmit:function(){
			
                },
                success:function(d){
                    d = $.parseJSON(d);
                    form.parse_data(d.data);
                    notification.show(d,'#'+form.settings.form.attr('id')+' ',1000);
                }
            })
		})
    }
	
	
        this.parse_data = function(dt)
        {
            $.each(dt,function(k,v){
                    if(k=='images'){
                      $.each(v,function(i,j)
                      {
                          $('#'+i).attr('src',j);
                      })
                    }
                    $('#'+k).val(v);
            })

        }
        
        this.parse_html = function(html){
            if(html)
            {
                $.each(html,function(k,v){
                    $('#'+k).html(v);
                })

                form.init();
            }
        }
        
	this.init_validators = function(form){
	
	}
        
        this.init_fields = function(form)
        {
            
        }
}

var form = new FormApi();

jQuery(function($){
form.init();
});
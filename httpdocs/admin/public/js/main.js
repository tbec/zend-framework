jQuery(function($){
//===== CHART LINEAR =====//
	if(jQuery('#chart_linear').length) /*Check if the element exists*/
	{
		var sin = [], cos = [];
		
		for (var i = 0; i < 10; i += 0.5) {
			sin.push([i, Math.sin(i)]);
			cos.push([i, Math.cos(i)]);
		}
		
		var plot = $.plot($("#chart_linear"), [ { data: sin, label: "sin(x)"}, { data: cos, label: "cos(x)" } ], {
			series: {
				lines: { show: true },
				points: { show: true }
			},
			grid: { hoverable: true, clickable: true },
			yaxis: { min: -1.1, max: 1.1 },
			xaxis: { min: 0, max: 9 }
		});
		
		function showTooltip(x, y, contents)
		{
			$('<div id="tooltip" class="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 5,
				
				padding: '6px 5px',
				'z-index': '9999',
				'background-color': '#31383a',
				'color': '#fff',
				'font-size': '11px',
				opacity: 0.85,
				'border-radius': '3px',
				'-webkit-border-radius': '3px',
				'-moz-border-radius': '3px'
			}).appendTo("body").fadeIn(200);
		}
		
		var previousPoint = null;
		
		$("#chart_linear").bind("plothover", function (event, pos, item){
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));
			if ($("#chart_linear").length > 0)
			{
				if(item)
				{
					if(previousPoint != item.dataIndex)
					{
						previousPoint = item.dataIndex;
						$("#tooltip").remove();
						var x = item.datapoint[0].toFixed(2), y = item.datapoint[1].toFixed(2);
						showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
					}
				}
				else
				{
					$("#tooltip").remove();
					previousPoint = null;
				}
			}
		});
		
		$("#chart_linear").bind("plotclick", function (event, pos, item){
			if(item) {
				$("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
				plot.highlight(item.series, item.datapoint);
			}
		});
	}
	
	
//===== CHART -  AUTOUPDATE =====//
	if(jQuery('#auto_update').length) /*Check if the element exists*/
	{
		// we use an inline data source in the example, usually data would
		// be fetched from a server
		var data = [], totalPoints = 300;
		function getRandomData() {
			if (data.length > 0)
				data = data.slice(1);
	
			// do a random walk
			while (data.length < totalPoints) {
				var prev = data.length > 0 ? data[data.length - 1] : 50;
				var y = prev + Math.random() * 10 - 5;
				if (y < 0)
					y = 0;
				if (y > 100)
					y = 100;
				data.push(y);			
			}
	
			// zip the generated y values with the x values
			var res = [];
			for (var i = 0; i < data.length; ++i)
				res.push([i, data[i]])
			return res;
		}
	
		// setup control widget
		var updateInterval = 120;
		$("#updateInterval").val(updateInterval).change(function () {
			var v = $(this).val();
			if (v && !isNaN(+v)) {
				updateInterval = +v;
				if (updateInterval < 1)
					updateInterval = 1;
				if (updateInterval > 2000)
					updateInterval = 2000;
				$(this).val("" + updateInterval);
			}
		});
	
		// setup plot
		var options = {
			series: { shadowSize: 0 }, // drawing is faster without shadows
			yaxis: { min: 0, max: 100 },
			xaxis: { show: false }
		};
		var plot = $.plot($("#auto_update"), [ getRandomData() ], options);
	
		function update() {
			plot.setData([ getRandomData() ]);
			// since the axes don't change, we don't need to call plot.setupGrid()
			plot.draw();
			
			setTimeout(update, updateInterval);
		}
	
		update();	
	}

	/*
	//===== jQUERY DATA TABLE =====//			
	oTable = $('#jqtable').dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers"
	});
	*/
	//===== RESPONSIVE NAV =====//	
	jQuery(".res_icon").toggle(function() {
		 $('#responsive_nav').slideDown(300);	
		 }, function(){
		 $('#responsive_nav').slideUp(300);		 
	})
	
	
	//messages
	$("div.msgbar").click(function(){
			$(this).slideUp();
	});

        initTip();
})

//===== TOOLTIP =====//
function initTip()
{
	jQuery('.tip_north').tipsy({gravity: 's'});
	jQuery('.tip_south').tipsy({gravity: 'n'});
	jQuery('.tip_east').tipsy({gravity: 'e'});
	jQuery('.tip_west').tipsy({gravity: 'w'});
}

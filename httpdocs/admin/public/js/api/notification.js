var NotificationApi = function(){
    this.settings = {}
	
    this.clear = function()
    {
            $('#msgs').fadeOut()
    }
	
    this.show = function(d,form,timeout){
		if(d.errors)
		{
                        $('.validity-tooltip').hide();	
			$.each(d.errors,function(k,v){
                            if (!$(form+' #e_'+k).length) $(form+' #'+k).parent().append('<div class="validity-tooltip" id="e_'+k+'" style="right:0px; bottom:0px">'+v+'<div class="validity-tooltip-outer"><div class="validity-tooltip-inner"></div></div></div>')
                            else $(form+' #e_'+k).show();
                            $(form+' #'+k).focus(function(){ $(form+' #e_'+k).hide() })
			})
		}
	
        $('#msgs').html('<div style="display:none" class="msgbar msg_'+((d.type)?'Success':'Error')+' hide_onC" onclick="jQuery(this).slideUp()"><span class="iconsweet">=</span><p>'+d.msg+'</p></div>');
		
		$('#msgs .msgbar').show();
		$('#msgs').fadeIn()
		if(timeout) setTimeout(function(){ $('#msgs').fadeOut() },timeout);
    }
}

var notification = new NotificationApi();
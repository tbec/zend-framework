<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
		$this -> initAcl($e);
        $application = $e->getApplication();
        #$application->getServiceManager()->get('translator');
        #$application->getServiceManager()->get('navigation');
        $eventManager = $application->getEventManager();
        $eventManager->attach('route', array($this, 'checkAcl'));
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
		
        $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
            $controller = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config = $e->getApplication()->getServiceManager()->get('config');
            if (isset($config['module_layouts'][$moduleNamespace])) {
                $controller->layout($config['module_layouts'][$moduleNamespace]);
            }
        }, 100);
    }
	
	
    public function getConfig()
    {
        return include __DIR__ . '../../'.__NAMESPACE__.'/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
	
	
	public function initAcl(MvcEvent $e) {
		$acl = new \Zend\Permissions\Acl\Acl();
		$roles = include __DIR__ . '/config/module.acl.roles.php';
		$allResources = array();
		foreach ($roles as $role => $resources) {
	 
			$role = new \Zend\Permissions\Acl\Role\GenericRole($role);
			$acl -> addRole($role);
	 
			$allResources = array_merge($resources, $allResources);
	 
			foreach ($resources as $resource) {
				 if(!$acl ->hasResource($resource))
					$acl -> addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
			}
			foreach ($allResources as $resource) {
				$acl -> allow($role, $resource);
			}
		}
		$e -> getViewModel() -> acl = $acl;
	 
	}
	 
	public function checkAcl(MvcEvent $e) {
		
		$r = $e -> getRouteMatch();
            
		$route = $r->getParam('controller');
		$userRole = 'guest';
		$acl = $e -> getViewModel() -> acl;

		try {
			$e -> getViewModel() -> acl -> isAllowed($userRole, $route);
		} catch(\Zend\Permissions\Acl\Exception\InvalidArgumentException $er) {
			echo $er->getMessage();
			exit();
			$response = $e -> getResponse();
			$response -> getHeaders() -> addHeaderLine('Location', $e -> getRequest() -> getBaseUrl() . '/account/login');
			$response -> setStatusCode(303);
	 
		}
	}
}

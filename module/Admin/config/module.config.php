<?php
namespace Admin;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonAdmin for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'admin' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
						'__NAMESPACE__' => __NAMESPACE__.'\Controller',
						'module' =>strtolower(__NAMESPACE__),
                        'controller' =>'index',
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '[:controller[/[:action[/]]]]',
						),
						'may_terminate' => true,
						'child_routes' => array(
							'wildcard' => array(
								'type' => 'Wildcard',
							),
						),
					),
				),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Admin\Navigation\NavigationFactory'
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
       'invokables' => array(
            __NAMESPACE__.'\Controller\Index' => __NAMESPACE__.'\Controller\IndexController',
            __NAMESPACE__.'\Controller\News' => __NAMESPACE__.'\Controller\NewsController',
            __NAMESPACE__.'\Controller\Pages' => __NAMESPACE__.'\Controller\PagesController',
            __NAMESPACE__.'\Controller\Account' => __NAMESPACE__.'\Controller\AccountController',
            __NAMESPACE__.'\Controller\Products' => __NAMESPACE__.'\Controller\ProductsController',
            __NAMESPACE__.'\Controller\Error' => __NAMESPACE__.'\Controller\ErrorController',
            __NAMESPACE__.'\Controller\Categories' => __NAMESPACE__.'\Controller\CategoriesController',
            __NAMESPACE__.'\Controller\Products' => __NAMESPACE__.'\Controller\ProductsController',
            __NAMESPACE__.'\Controller\Accounts' => __NAMESPACE__.'\Controller\AccountsController',
            __NAMESPACE__.'\Controller\Banners' => __NAMESPACE__.'\Controller\BannersController',
           __NAMESPACE__.'\Controller\Upload' => __NAMESPACE__.'\Controller\UploadController'
        ),
    ),
    'view_manager' => array(
        'defaultSuffix' =>'tpl',
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/admin'           => __DIR__ . '/../view/layout/layout.tpl',
			'layout/custom' 		  => __DIR__ . '/../view/layout/custom.tpl',	
            'error/404'               => __DIR__ . '/../view/error/404.tpl',
            'error/index'             => __DIR__ . '/../view/error/index.tpl',
			'pagination' 			  => __DIR__ . '/../view/admin/partials/pagination/p2.tpl',
		),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        )
    ),
// Doctrine config
    'doctrine' => array(
       'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Admin/Entity'),
            ),
			
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
		'authentication' => array(
		'orm_default' => array(
			//should be the key you use to get doctrine's entity manager out of zf2's service locator
			'objectManager' => 'Doctrine\ORM\EntityManager',
			//fully qualified name of your user class
			'identityClass' => 'Admin\Entity\XcAccounts',
			//the identity property of your class
			'identityProperty' => 'login',
			//the password property of your class
			'credentialProperty' => 'password',
			//a callable function to hash the password with
			#'credentialCallable' => 'Application\Model\XcAccounts::hashPassword'
		),
		)
    ),
	'module_layouts' => array(
        'Admin' => 'layout/admin',
    )
);
<?php
namespace Admin;
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
'guest'=> array(
	'index',
	'pages',
        'categories',
	'account',
        'accounts',
        'news',
        'products',
        'banners',
        'upload'
),
'admin'=> array(
	'add-user',
	'delete-user'
));
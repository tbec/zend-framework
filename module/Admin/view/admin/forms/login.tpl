<div class="widget login_form" style="width:350px; margin:auto; margin-top:100px">
    <div class="widget_title"><span class="iconsweet">r</span><h5>Login form</h5></div>
    <div class="widget_body">
		<!--Form fields-->
        {$this->form()->openTag($form)}
        <ul class="form_fields_container">
            
            
                {foreach from=$form item=f}
                <li>
                        
                        <label>{$f->getLabel()}</label>
                        <div class="form_input {if $f->getAttribute('type') =='submit'}submit{/if}" style="width:200px">
                        {$this->formElement($f)}
                        {if $this->formElementErrors($f)}
                                <div class="validity-tooltip" id="e_{$f->getAttribute('id')}" style="right:0px; bottom:0px">{$this->formElementErrors($f)}<div class="validity-tooltip-outer"><div class="validity-tooltip-inner"></div></div></div>
                        {/if}
                        </div>
                        
                </li>
                {/foreach}    
        </ul>
        {$this->form()->closeTag()}
    </div>
</div>


	
	
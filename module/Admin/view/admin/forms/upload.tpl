<!--One_Wrap-->
<div class="one_wrap">
	<div class="widget">
		<div class="widget_title"><span class="iconsweet">r</span><h5>Uploader</h5></div>
		<div class="widget_body">
			<!--Form fields-->
			{$this->form()->openTag($form)}
                        <ul class="tabs-holder">
                                {foreach $tabs key=k item=t}
                                <li><a id="tab_{$k}" href="#tab_{$k}">{$t}</a></li>
                                {/foreach}
                        </ul>
                        
			<div id="tabs-1" class="tab_content">
			  
				<ul class="form_fields_container">
						{foreach from=$form item=f}
							{assign var=type value=$fields[$f->getAttribute('id')]['type']}
							{include file="../forms/elements/{$f->getAttribute('type')}.tpl" c=$fields[$f->getAttribute('id')] f=$f}  
						{/foreach}
				</ul>
			
			</div> 
			{$this->form()->closeTag()}
		</div>
	</div>
</div>
<li class="{if $f->getAttribute('hidden')}hide{/if} tab_{$c['tab']|string_format:"%d"}"><label>{$f->getLabel()}</label>
    <div class="form_input">  
        
        <select name="{$f->getAttribute('name')}" class="{$f->getAttribute('class')}" {if $f->getAttribute('disabled')}disabled="disabled"{/if}>
		<option value="0">Default</option>
        {foreach $c.options.value_options as $v=>$k}
        {if $smarty.get.id!=$v}
		<option {if $f->getValue()==$v}selected="selected"{/if} value="{$v}">
        {$k}
        </option>
		{/if}
        {/foreach}
        </select>
      
    </div>
</li>
<li class="{if $f->getAttribute('hidden')}hide{/if} tab_{$c['tab']|string_format:"%d"}"><label>{$f->getLabel()}</label>
    <div class="form_input buttonset">  
        {if $c.options.value_options}
        {foreach $c.options.value_options as $v=>$k}
        <label>
        <input name="{$f->getAttribute('name')}" class="{$f->getAttribute('class')}" {if $f->getAttribute('disabled')}disabled="disabled"{/if} type="checkbox" {if $f->getValue()==$v}checked="checked"{/if} value="{$v}"/>
        {$k}
        </label>
        {/foreach}
        {else}
        <input id="{$f->getAttribute('id')}" name="{$f->getAttribute('name')}" class="{$f->getAttribute('class')}" {if $f->getAttribute('disabled')}disabled="disabled"{/if} type="checkbox" {if $f->getValue()}checked="checked"{/if} value="1"/>
        {/if}
    </div>
</li>
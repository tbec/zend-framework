<li class="{if $f->getAttribute('hidden')}hide{/if} tab_{$c['tab']|string_format:"%d"}"><label>{$f->getLabel()}</label>
    <div class="form_input">  
        <textarea id="{$f->getAttribute('id')}" name="{$f->getAttribute('name')}" class="{$f->getAttribute('class')}" {if $f->getAttribute('disabled')}disabled="disabled"{/if}>{$f->getValue()}</textarea>
    </div>
</li>
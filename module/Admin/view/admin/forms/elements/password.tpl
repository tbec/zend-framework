<li class="{if $f->getAttribute('hidden')}hide{/if} tab_{$c['tab']|string_format:"%d"}"><label>{$f->getLabel()}</label>
    <div class="form_input">  
        <input id="{$f->getAttribute('id')}" name="{$f->getAttribute('name')}" class="{$f->getAttribute('class')}" {if $f->getAttribute('disabled')}disabled="disabled"{/if} type="password" value=""/>
    </div>
</li>
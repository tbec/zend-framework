<li class="{if $f->getAttribute('hidden')}hide{/if} tab_{$c['tab']|string_format:"%d"}">
	<label>{$f->getLabel()}</label>
	
    <div class="form_input">  
        <img id="thumbnail" src="{$basePath}/../../uploads/{$f->getValue()|@replace:'[size]':'128_128'}" />
        
        <input  name="{$f->getAttribute('name')}" class="{$f->getAttribute('class')}" style="padding:0px; border:none" {if $f->getAttribute('disabled')}disabled="disabled"{/if} type="file" value="{$f->getValue()}"/>
    </div>
</li>
{capture name="scripts"}
    {$this->headLink()->appendStylesheet("`$basePath`/admin/public/css/upload.css")}
    {$this->headScript()->appendFile("`$basePath`/admin/public/js/uploader/jquery.uploadifive.js", "text/javascript")}
{/capture}

<li {if $f->getAttribute('hidden')}class="hide"{/if}><label>{$f->getLabel()}</label>
    <div class="form_input">  
    <div style="float:left; position:relative; width:100%">
    <div style="padding-right:10px">
    <div style="position:relative">
        <div class="ovinfo">
            Drop images here or
            <input id="file_upload_{$f->getAttribute('id')}" name="file_upload" type="file" class="file_upload" multiple="true">
        </div>
        <div class="queue"></div>
    </div>

    <div id="queue"></div>
   {* <a style="position: relative; top: 8px;" href="javascript:$('.file_upload').uploadifive('upload')">Upload Files</a>*}
    </div>
    </div>

    <script type="text/javascript">
            $(function() {
                    $('#file_upload_{$f->getAttribute('id')}').uploadifive({
                            'auto'             : false,
                            'formData'         : { jid:0 },
                            'method'           : 'post',
                            'queueID'          : 'queue',
                            'fileType'         : 'image',
                            'multi'            : {$c.options.multi},
                            'fileObjName'      : '{$f->getAttribute('name')}',
                            'fileSizeLimit'    : {$c.options.uploadSize},
                            'onUploadComplete' : function(file, data) { console.log(data); }
                    });
            });
    </script>
    </div>
</li>
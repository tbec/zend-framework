<li class="{if $f->getAttribute('hidden')}hide{/if} tab_{$c['tab']|string_format:"%d"}"><label>{$f->getLabel()}</label>
	<div class="form_input">
	<input name="{$f->getAttribute('name')}" class="{$f->getAttribute('class')} switch" {if $f->getAttribute('disabled')}disabled="disabled"{/if} type="checkbox" {if $f->getValue()==1}checked="checked"{/if} value="1" />

	{literal}
	<script type="text/javascript">
		jQuery(function($){
		 // Replace checkboxes with switch
			$("input[type=checkbox].switch").each(function() {

					// Insert switch
					$(this).before('<span class="switch"><span class="background" /><span class="mask" /></span>');

					// Hide checkbox
					$(this).hide();

					// Set inital state
					if (!$(this)[0].checked) $(this).prev().find(".background").css({left: "-56px"});
			});

			$("span.switch").click(function() {
					
					// Slide switch off
					if ($(this).next()[0].checked) {
							$(this).find(".background").animate({left: "-56px"}, 200);
							
							
					// Slide switch on
					} else {
							$(this).find(".background").animate({left: "0px"}, 200);
							
					}

					// Toggle state of checkbox
					$(this).next()[0].checked = !$(this).next()[0].checked;
			});
		})
	</script>
	{/literal}
	</div>
</li>
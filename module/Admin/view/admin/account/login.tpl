{capture name="scripts"}
{$this->headLink()
->appendStylesheet("`$basePath`/public/admin/css/login.css")
}
{$this->headScript()
->appendFile("/js/api/notification.js", "text/javascript")
->appendFile("/js/jquery.validity.js", "text/javascript")
->appendFile("/js/jquery.form.js", "text/javascript")
->appendFile("public/admin/js/login.js", "text/javascript")
->appendFile("/js/api/user.js", "text/javascript")}
{/capture}

{* login-form *}
{include file="../forms/login.tpl" grid=6 push=5}
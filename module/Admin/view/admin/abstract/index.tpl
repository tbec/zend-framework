    <!--Quick Actions-->
    <div id="quick_actions"><a class="button_big" href="{$basePath}/admin/{$controller}/add"><span class="iconsweet">+</span>{$labels.add}</a></div>
    <!--One_Wrap-->
    <div class="one_wrap">
        <!--Datatable-->
        <div class="widget">
            <div class="widget_title"><span class="iconsweet">s</span>
                <h5>{$labels.title}</h5>
            </div>
            <div class="widget_body">
                <div class="demo_jui dataTables_wrapper">
                    
                    <div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"><div class="dataTables_filter" id="jqtable_filter"><label>Search: <input type="text" aria-controls="jqtable"></label></div></div>
                    
                    <table cellpadding="0" cellspacing="0" border="0" class="display dataTable" id="jqtable">
                        <thead>
                            <tr>
                                {foreach from=$fields item=f key=k}
									{if $f['list']}
                                    <th><a href="/admin/{$controller}?sort={$k}&ord={if $sort==$k && $ord=='asc'}desc{else}asc{/if}" class="DataTables_sort_wrapper">{$f['label']}<span class="DataTables_sort_icon css_right ui-icon ui-icon-{if $k==$sort && $ord=='asc'}triangle-1-n{elseif $k==$sort && $ord=='desc'}triangle-1-s{else}carat-2-n-s{/if}"></span></a></th>
									{/if}
								{/foreach}
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                    {foreach $paginator key=ii item=i}
                            <tr class="gradeA {if $ii%2==0}even{else}odd{/if}">
                            {foreach from=$fields item=f key=k}
								{if $f['list']}
                                <td {if $f['indent'] && $i->get('depth')>0}style="text-indent:{$i->get('depth')*$f['indent']}px"{/if}>{$i->get($k)}</td>
								{/if}
                            {/foreach}
                                <td>
                                    <span class="data_actions iconsweet">
                                        <a class="tip_north" original-title="Delete" href="{$controller}/delete?id={$i->get($primary)}">X</a>
                                        <a class="tip_north" original-title="Edit" href="{$controller}/edit?id={$i->get($primary)}">C</a> 
                                    </span>
                                </td>
                            </tr>
                    {/foreach}
                    
                        </tbody>
                        
                    </table>
                    
        {$this->paginationControl($this->paginator,'sliding','pagination')}
                </div>
            </div>
        </div>
                
    </div>
    <!--One_Wrap-->
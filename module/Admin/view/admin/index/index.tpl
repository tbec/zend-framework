{capture name="scripts"}
    {$this->headScript()
    ->appendFile("`$basePath`/admin/public/js/excanvas.js", "text/javascript")
    ->appendFile("`$basePath`/admin/public/js/jquery.flot.js", "text/javascript")}
{/capture}

     <!--Activity Stats-->
      
      <div id="activity_stats">
        <h3>Activity</h3>
        <div class="activity_column"> <span class="iconsweet">+</span> <span class="big_txt rd_txt">12</span> views </div>
        <div class="activity_column"> <span class="iconsweet">Y</span> <span class="big_txt gr_txt">6</span> visits </div>
        <div class="activity_column"> <span class="iconsweet">#</span> <span class="big_txt">2</span> messages </div>
      </div>
      
      <!--Quick Actions-->
      
      <div id="quick_actions"><a class="button_big" href="#"><span class="iconsweet">w</span>Export report</a></div>
      
      
      <!--One_TWO-->
      
      <div class="one_two_wrap fl_left">
        <div class="widget">
          <div class="widget_title"><span class="iconsweet">r</span>
            <h5>Visitors</h5>
          </div>
          <div class="widget_body">
            <div class="content_pad"> 
              
              <div id="chart_linear" class="no_overflow"  style="width:100%;height:270px"></div>
            </div>
          </div>
        </div>
      </div>
      
      <!--One_TWO-->
      
      <div class="one_two_wrap fl_right">
        <div class="widget">
          <div class="widget_title"><span class="iconsweet">t</span>
            <h5>Statistics</h5>
          </div>
          <div class="widget_body"> 
            
            <!--Stastics-->
            
            <ul class="dw_summary">
              <li> <span class="percentage_done">12%</span> New registrations
                <div class="progress_wrap">
                  <div title="12%" class="tip_north progress_bar" style="width:12%"></div>
                </div>
              </li>
              <li> <span class="percentage_done">9%</span> Re-opened tasks
                <div class="progress_wrap">
                  <div title="9%" class="tip_north progress_bar" style="width:9%"></div>
                </div>
              </li>
              <li> <span class="percentage_done">46%</span> Task completed
                <div class="progress_wrap">
                  <div title="46%" class="tip_north progress_bar" style="width:46%"></div>
                </div>
              </li>
              <li> <span class="percentage_done">82%</span> New visitors
                <div class="progress_wrap">
                  <div title="82%" class="tip_north progress_bar" style="width:82%"></div>
                </div>
              </li>
              <li> <span class="percentage_done">27%</span> Unique visitors
                <div class="progress_wrap">
                  <div title="27%" class="tip_north progress_bar" style="width:27%"></div>
                </div>
              </li>
              <li> <span class="percentage_done">0%</span> Affiliate registration
                <div class="progress_wrap">
                  <div title="0%" class="tip_north progress_bar" style="width:0%"></div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      
      <!--One_Wrap-->
      
      <div class="one_wrap">
        <div class="widget">
          <div class="widget_title"><span class="iconsweet">f</span>
            <h5>Members</h5>
          </div>
          <div class="widget_body">
            <div class="project_sort">
              <ul class="filter_project">
                <li class="segment  selected"><a class="all" href="#">All <span class="count">1</span></a></li>
                <li class="segment"><a class="user" href="#">User<span class="count">1</span></a></li>
                <li class="segment"><a class="Resolved" href="#">Administrators<span class="count">0</span></a></li>
              </ul>
              <ul class="project_list">
                <li data-id="id-1" data-type="user">
                    <a href="#" class="project_title" style="position:relative; z-index:2"> Tomasz Bęc</a> 
                    <a href="#" class="assigned_user" style="z-index:2"> <span class="iconsweet">a</span>User</a> 
                     <img src="/admin/public/admin/images/avatar.png" style="top:5px; left:8px; position:absolute" />
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      
      
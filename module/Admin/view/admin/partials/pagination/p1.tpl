{if $this->pageCount>1}
<!--Pagination--> 
<div class="widget fl_right">
       
            <div class="content_pad text_center">
                <ul class="pagination">
                    {if $this->previous}
                    <li>
                        <a href="{$controller}?page={$this->previous}">
                            &lt;
                        </a>
                    </li>
                    {/if}
                    {foreach from=$this->pagesInRange item=page}
                    <li>
                        <a {if $page == $this->current}class="active"{/if} href="{$controller}?page={$page}">
                            {$page}
                        </a>
                    </li>
                    {/foreach}
                    {if $this->next}
                    <li>
                        <a href="{$controller}?page={$this->next}">
                            &gt;
                        </a>
                    </li>
                    {/if}
               </ul>
            </div>
        </div>
</div>    
{/if}
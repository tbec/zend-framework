
<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
    <div class="dataTables_info" id="jqtable_info">
          Showing 1 to 10 of 10 entries
    </div>
{if $this->pageCount>1}
<div class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers" id="jqtable_paginate">
    <a class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" id="jqtable_first">First</a>
    {if $this->previous}<a href="{$controller}?page={$this->previous}" class="previous fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" id="jqtable_previous">Previous</a>{/if}
    <span>
        {foreach from=$this->pagesInRange item=page}
            <a class="fg-button ui-button ui-state-default {if $page == $this->current}ui-state-disabled{/if}" href="{$controller}?page={$page}">{$page}</a>
        {/foreach}
    </span>
   {if $this->next}<a href="{$controller}?page={$this->next}" class="next fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" id="jqtable_next">Next</a>{/if}
    <a class="last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" id="jqtable_last">Last</a>
</div>  
{/if}
</div>
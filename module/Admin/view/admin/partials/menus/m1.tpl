<!--Primary Navigation-->

<nav id="primary_nav">
  <ul>
    <li class="nav_dashboard {if $controller=='index'}active{/if}"><a href="/admin/index">Dashboard</a></li>
    <li class="nav_pages {if in_array($controller,array('pages','news'))}active{/if}"><a href="/admin/pages">Content</a></li>
    <li class="nav_pages {if in_array($controller,array('categories','products'))}active{/if}"><a href="/admin/products">Products</a></li>
    <li class="nav_pages {if in_array($controller,array('accounts'))}active{/if}"><a href="/admin/accounts">Accounts</a></li>
  </ul>
</nav>


  <!--Secondary Navigation-->

  <nav id="secondary_nav"> 

    <!--UserInfo-->

    <dl class="user_info">
      <dt><a href="{$basePath}/account"><img src="{$basePath}/public/images/avatar.png" alt="" /></a></dt>
      <dd> <a class="welcome_user" href="{$basePath}/account">Welcome, <strong>{if $user}{$user->getLogin()}{/if}</strong></a> <span class="log_data">Last sign in : 16:11 Feb 27th 2012</span> <a class="logout" href="{$basePath}/account/logout">Logout</a> <a class="user_messages" href="#"><span>12</span></a> </dd>
    </dl>

    <!--Responsive Nav--> 

    <a class="res_icon" href="#"></a>
    <ul id="responsive_nav">
      <li> <a href="/admin">Dashboard</a> </li>
      <li> <a href="/admin/pages">Pages</a></li>
    </ul>

    <!--Responsive Nav ends-->
	{if in_array($controller,array('pages','news'))}
		<h2>Content</h2>
		<ul>
		  <li><a href="/admin/pages"><span class="iconsweet">a</span>Pages</a></li>
		  <li><a href="/admin/news"><span class="iconsweet">k</span>News</a></li>
		</ul>
	{elseif in_array($controller,array('categories','products'))}
		<h2>Products</h2>
		<ul>
		  <li><a href="/admin/products"><span class="iconsweet">k</span>Products</a></li>
		  <li><a href="/admin/categories"><span class="iconsweet">a</span>Categories</a></li>
		</ul>
	{elseif in_array($controller,array('accounts'))}
		<h2>Accounts</h2>
		<ul>
		  <li><a href="/admin/accounts?type=0"><span class="iconsweet">a</span>Users</a></li>
		  <li><a href="/admin/accounts?type=1"><span class="iconsweet">a</span>Administrators</a></li>
		</ul>
	{else}
    <h2>Dashboard</h2>
    <ul>
      <li><a href="#"><span class="iconsweet">a</span>User Profiles</a></li>
      <li><a href="#"><span class="iconsweet">k</span>Admin Tools</a></li>
      <li><a href="#"><span class="iconsweet">o</span>Milestornes</a></li>
      <li><a href="#"><span class="iconsweet">S</span>Worklog</a></li>
    </ul>
	{/if}
</nav>
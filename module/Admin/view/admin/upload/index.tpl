<script type="text/javascript">
function image_settings()
{
	$('#image_form').modal();
}
</script>

<style type="text/css">
#file_uploader{ height:400px; background-color:#fff; max-height:400px; overflow-y:scroll; }

.media_library.grid { margin-top:10px; width:100%; float:left; }
.media_library.grid .image { position:relative; border:solid 1px #eee; cursor:pointer; margin-right:10px; float:left; background-color:#fff; display:block; }
.media_library.grid .image .i { background-color:#fff; background-repeat:no-repeat; margin:auto; background-position:center center; width:116px; height:116px; overflow:hidden; }

.media_library.grid .image .tools { background-color:#fff; opacity:.8; position:absolute; top:0px; width:100%;text-align:right; }
.media_library.grid .image .tools a,.media_library.grid .image .tools span {  text-decoration:none; font-size:18px }
.media_library.grid .image .tools input { float:left }
</style>


<div class="one_wrap fl_left">
    <div class="widget">
      <div class="widget_title"><span class="iconsweet">]</span>
        <h5>Media library</h5>
      </div>
      <div class="widget_body">  
        <div id="file_uploader" style="position:relative">
			<div class="filters">
				<select>
					<option value="image">Images</option>
				</select>
				
				
				<input type="text" value="" name="s" placeholder="search"/>
			</div>
			<div class="media_library grid">
				{foreach from=$images item=i}
				<label class="image">
				<div class="i" style="background-image:url({$basePath}/../../uploads/{$i['path']}116_116_{$i['name']})"></div>
				<div class="tools">
					<input type="checkbox" value="1" name="image[{$i['id']}]" />
					<span original-title="Settings" onclick="return image_settings()" class="iconsweet tip_north">k</span>
					<span original-title="Delete" style="font-size:14px; top:-2px; margin-right:5px; position:relative" class="iconsweet tip_north">X</span>
				</div>
				</label>
				{/foreach}
			</div>
			
			
        </div>
        <div class="action_bar"> 
			<a href="javascript:$('#file_upload').uploadifyUpload();" class="button_small whitishBtn" ><span class="iconsweet">o</span>Upload Files</a> 
			<input type="submit" class="button_small bluishBtn" value="Select">
		</div>
      </div>
    </div>
</div>
</div>


<form class="modal hide" id="image_form" >
    <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Modal header</h3>
    </div>
    <div class="modal-body">
             
			 <!--One_Wrap-->
			<div class="one_wrap">
				<div class="widget" style="box-shadow:none">
					
						<ul class="form_fields_container">
							<li class=" tab_0"><label>Alt</label>
								<div class="form_input">  
									<input type="text" value="" class="" name="route" id="route">
								</div>
							</li> 
						</ul>
				</div>
			</div>
			 
    </div>
    <div class="modal-footer">
    <a data-dismiss="modal" class="greyishBtn button_small" href="#">Close</a>
    <input type="submit" class="button_small bluishBtn" value="Save changes">
    </div>
</form>

{$this->doctype()}

<html lang="en">
<head>
    <meta charset="utf-8">
    {$this->headTitle()->setSeparator(' - ')->setAutoEscape(false)}

    {$basePath = $this->basePath()}
	
	{capture name="scripts"}
    {$this->headLink([ 'rel' => 'shortcut icon', 'type' => 'image/vnd.microsoft.icon', 'href' =>
    "`$basePath`/images/favicon.ico"])}
	
    {$this->headTitle($this->seo['title'])}
	{/capture}
    {$this->headMeta()}

    <!-- Styles -->
    {$this->headLink()}

    <!-- Scripts -->
    <script type="text/javascript">
        var base = '{$basePath}';
    </script>
	{$this->headStyle()}	
    {$this->headScript()}	

</head>

<body>    
<!--Header-->

<header> 
  
  <!--Logo-->
  
  <div id="logo"><a href="{$basePath}/"><img src="{$basePath}/public/images/logo.png" alt="" /></a></div>
  
  <!--Search-->
  
  <div class="header_search">
    <form action="#">
      <input type="text" name="search" placeholder="Search" id="ac">
      <input type="submit" value="">
    </form>
  </div>
</header>

<!--Dreamworks Container-->

<div id="dreamworks_container"> 
  
  {include file='../admin/partials/menus/m1.tpl'}  
  
  <!--Main Content-->
  
  <section id="main_content">  
    <div id="content_wrap">
      <div id="msgs">
       
      </div>
      <div id="content">
        {$this->content}
      </div>
    </div>
  </section>
</div>
</body>
</html>

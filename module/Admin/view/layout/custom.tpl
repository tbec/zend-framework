{$this->doctype()}

<html lang="en">
<head>
    <meta charset="utf-8">
    {$this->headTitle()->setSeparator(' - ')->setAutoEscape(false)}

    {$basePath = $this->basePath()}
    {capture name="scripts"}
    {$this->headLink()
    ->appendStylesheet("`$basePath`/public/admin/css/reset.css")
    ->appendStylesheet("`$basePath`/public/admin/css/main.css")}
    {$this->headLink([ 'rel' => 'shortcut icon', 'type' => 'image/vnd.microsoft.icon', 'href' =>
    "`$basePath`/images/favicon.ico"])}
    
    {$this->headScript()
    ->prependFile("`$basePath`/js/jquery.min.js", "text/javascript")
    ->appendFile("`$basePath`/js/jquery_ui/jquery-ui-1.10.2.custom.js", "text/javascript")}
    {$this->headTitle($this->seo['title'])}
    {/capture}
    {*$this->headMeta()*}

    <!-- Styles -->
    {$this->headLink()}

    <!-- Scripts -->
    {$this->headScript()}	
	

</head>

<body>   
  <!--Header-->

    <header> 

      <!--Logo-->
      <div class="loader" style="position:fixed; display:none; top:0px; left:0px; z-index:200; width:100%; height:100%; background:#000; opacity:.5">
        <div style=" position:absolute; top:258px; left:50%; z-index:200">
            <img src="{$basePath}/public/images/loaders/load5.gif" alt="" />
        </div>
      </div>
     <div id="logo"><a href="{$basePath}/"><img src="{$basePath}/public/images/logo.png" alt="" /></a></div>
    </header>  
    <div id="msgs">
       
      </div>
  <!--Main Content-->
    
      {$this->content}

</body>
</html>

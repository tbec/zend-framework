<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcManufacturersLang
 *
 * @ORM\Table(name="xc_manufacturers_lang")
 * @ORM\Entity
 */
class XcManufacturersLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_producenta", type="bigint", nullable=false)
     */
    private $idProducenta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="id_lang", type="boolean", nullable=false)
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa", type="string", length=255, nullable=false)
     */
    private $nazwa;

    /**
     * @var string
     *
     * @ORM\Column(name="krotki_opis", type="string", length=255, nullable=false)
     */
    private $krotkiOpis;

    /**
     * @var string
     *
     * @ORM\Column(name="opis", type="text", nullable=false)
     */
    private $opis;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=300, nullable=false)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=300, nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=300, nullable=false)
     */
    private $metaTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProducenta
     *
     * @param integer $idProducenta
     * @return XcManufacturersLang
     */
    public function setIdProducenta($idProducenta)
    {
        $this->idProducenta = $idProducenta;
    
        return $this;
    }

    /**
     * Get idProducenta
     *
     * @return integer 
     */
    public function getIdProducenta()
    {
        return $this->idProducenta;
    }

    /**
     * Set idLang
     *
     * @param boolean $idLang
     * @return XcManufacturersLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;
    
        return $this;
    }

    /**
     * Get idLang
     *
     * @return boolean 
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     * @return XcManufacturersLang
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string 
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set krotkiOpis
     *
     * @param string $krotkiOpis
     * @return XcManufacturersLang
     */
    public function setKrotkiOpis($krotkiOpis)
    {
        $this->krotkiOpis = $krotkiOpis;
    
        return $this;
    }

    /**
     * Get krotkiOpis
     *
     * @return string 
     */
    public function getKrotkiOpis()
    {
        return $this->krotkiOpis;
    }

    /**
     * Set opis
     *
     * @param string $opis
     * @return XcManufacturersLang
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    
        return $this;
    }

    /**
     * Get opis
     *
     * @return string 
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return XcManufacturersLang
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return XcManufacturersLang
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return XcManufacturersLang
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return XcManufacturersLang
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
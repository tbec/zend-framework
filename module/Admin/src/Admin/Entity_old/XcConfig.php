<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcConfig
 *
 * @ORM\Table(name="xc_config")
 * @ORM\Entity
 */
class XcConfig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa_firmy", type="string", length=255, nullable=false)
     */
    private $nazwaFirmy;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=false)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="adres", type="string", length=255, nullable=false)
     */
    private $adres;

    /**
     * @var string
     *
     * @ORM\Column(name="kod_pocztowy", type="string", length=6, nullable=false)
     */
    private $kodPocztowy;

    /**
     * @var string
     *
     * @ORM\Column(name="miasto", type="string", length=255, nullable=false)
     */
    private $miasto;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefon", type="string", length=255, nullable=false)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics_id", type="string", length=255, nullable=false)
     */
    private $analyticsId;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics_email", type="string", length=255, nullable=false)
     */
    private $analyticsEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics_pass", type="string", length=255, nullable=false)
     */
    private $analyticsPass;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics_tracking", type="string", length=255, nullable=false)
     */
    private $analyticsTracking;

    /**
     * @var integer
     *
     * @ORM\Column(name="smtp", type="integer", nullable=false)
     */
    private $smtp;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_host", type="string", length=255, nullable=false)
     */
    private $smtpHost;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_login", type="string", length=255, nullable=false)
     */
    private $smtpLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_password", type="string", length=255, nullable=false)
     */
    private $smtpPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=255, nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=false)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=255, nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_cache", type="text", nullable=false)
     */
    private $profileCache;

    /**
     * @var string
     *
     * @ORM\Column(name="gallery", type="string", length=500, nullable=false)
     */
    private $gallery;

    /**
     * @var string
     *
     * @ORM\Column(name="modules", type="string", length=255, nullable=false)
     */
    private $modules;

    /**
     * @var integer
     *
     * @ORM\Column(name="wiadomosci", type="bigint", nullable=false)
     */
    private $wiadomosci;

    /**
     * @var integer
     *
     * @ORM\Column(name="liczba_odwiedzin", type="bigint", nullable=false)
     */
    private $liczbaOdwiedzin;

    /**
     * @var integer
     *
     * @ORM\Column(name="newsletter_count", type="integer", nullable=false)
     */
    private $newsletterCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="formularz_produktowy", type="integer", nullable=false)
     */
    private $formularzProduktowy;

    /**
     * @var string
     *
     * @ORM\Column(name="product", type="string", length=255, nullable=false)
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="listing", type="string", length=255, nullable=false)
     */
    private $listing;

    /**
     * @var string
     *
     * @ORM\Column(name="news", type="string", length=255, nullable=false)
     */
    private $news;

    /**
     * @var string
     *
     * @ORM\Column(name="boxes", type="string", length=255, nullable=false)
     */
    private $boxes;

    /**
     * @var string
     *
     * @ORM\Column(name="layout", type="string", length=255, nullable=false)
     */
    private $layout;

    /**
     * @var integer
     *
     * @ORM\Column(name="www_redirect", type="integer", nullable=false)
     */
    private $wwwRedirect;

    /**
     * @var string
     *
     * @ORM\Column(name="robots_txt", type="string", length=500, nullable=false)
     */
    private $robotsTxt;

    /**
     * @var string
     *
     * @ORM\Column(name="twoje_zaplecze", type="string", length=255, nullable=false)
     */
    private $twojeZaplecze;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwaFirmy
     *
     * @param string $nazwaFirmy
     * @return XcConfig
     */
    public function setNazwaFirmy($nazwaFirmy)
    {
        $this->nazwaFirmy = $nazwaFirmy;
    
        return $this;
    }

    /**
     * Get nazwaFirmy
     *
     * @return string 
     */
    public function getNazwaFirmy()
    {
        return $this->nazwaFirmy;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return XcConfig
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set adres
     *
     * @param string $adres
     * @return XcConfig
     */
    public function setAdres($adres)
    {
        $this->adres = $adres;
    
        return $this;
    }

    /**
     * Get adres
     *
     * @return string 
     */
    public function getAdres()
    {
        return $this->adres;
    }

    /**
     * Set kodPocztowy
     *
     * @param string $kodPocztowy
     * @return XcConfig
     */
    public function setKodPocztowy($kodPocztowy)
    {
        $this->kodPocztowy = $kodPocztowy;
    
        return $this;
    }

    /**
     * Get kodPocztowy
     *
     * @return string 
     */
    public function getKodPocztowy()
    {
        return $this->kodPocztowy;
    }

    /**
     * Set miasto
     *
     * @param string $miasto
     * @return XcConfig
     */
    public function setMiasto($miasto)
    {
        $this->miasto = $miasto;
    
        return $this;
    }

    /**
     * Get miasto
     *
     * @return string 
     */
    public function getMiasto()
    {
        return $this->miasto;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return XcConfig
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     * @return XcConfig
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    
        return $this;
    }

    /**
     * Get telefon
     *
     * @return string 
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Set analyticsId
     *
     * @param string $analyticsId
     * @return XcConfig
     */
    public function setAnalyticsId($analyticsId)
    {
        $this->analyticsId = $analyticsId;
    
        return $this;
    }

    /**
     * Get analyticsId
     *
     * @return string 
     */
    public function getAnalyticsId()
    {
        return $this->analyticsId;
    }

    /**
     * Set analyticsEmail
     *
     * @param string $analyticsEmail
     * @return XcConfig
     */
    public function setAnalyticsEmail($analyticsEmail)
    {
        $this->analyticsEmail = $analyticsEmail;
    
        return $this;
    }

    /**
     * Get analyticsEmail
     *
     * @return string 
     */
    public function getAnalyticsEmail()
    {
        return $this->analyticsEmail;
    }

    /**
     * Set analyticsPass
     *
     * @param string $analyticsPass
     * @return XcConfig
     */
    public function setAnalyticsPass($analyticsPass)
    {
        $this->analyticsPass = $analyticsPass;
    
        return $this;
    }

    /**
     * Get analyticsPass
     *
     * @return string 
     */
    public function getAnalyticsPass()
    {
        return $this->analyticsPass;
    }

    /**
     * Set analyticsTracking
     *
     * @param string $analyticsTracking
     * @return XcConfig
     */
    public function setAnalyticsTracking($analyticsTracking)
    {
        $this->analyticsTracking = $analyticsTracking;
    
        return $this;
    }

    /**
     * Get analyticsTracking
     *
     * @return string 
     */
    public function getAnalyticsTracking()
    {
        return $this->analyticsTracking;
    }

    /**
     * Set smtp
     *
     * @param integer $smtp
     * @return XcConfig
     */
    public function setSmtp($smtp)
    {
        $this->smtp = $smtp;
    
        return $this;
    }

    /**
     * Get smtp
     *
     * @return integer 
     */
    public function getSmtp()
    {
        return $this->smtp;
    }

    /**
     * Set smtpHost
     *
     * @param string $smtpHost
     * @return XcConfig
     */
    public function setSmtpHost($smtpHost)
    {
        $this->smtpHost = $smtpHost;
    
        return $this;
    }

    /**
     * Get smtpHost
     *
     * @return string 
     */
    public function getSmtpHost()
    {
        return $this->smtpHost;
    }

    /**
     * Set smtpLogin
     *
     * @param string $smtpLogin
     * @return XcConfig
     */
    public function setSmtpLogin($smtpLogin)
    {
        $this->smtpLogin = $smtpLogin;
    
        return $this;
    }

    /**
     * Get smtpLogin
     *
     * @return string 
     */
    public function getSmtpLogin()
    {
        return $this->smtpLogin;
    }

    /**
     * Set smtpPassword
     *
     * @param string $smtpPassword
     * @return XcConfig
     */
    public function setSmtpPassword($smtpPassword)
    {
        $this->smtpPassword = $smtpPassword;
    
        return $this;
    }

    /**
     * Get smtpPassword
     *
     * @return string 
     */
    public function getSmtpPassword()
    {
        return $this->smtpPassword;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return XcConfig
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return XcConfig
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return XcConfig
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set profileCache
     *
     * @param string $profileCache
     * @return XcConfig
     */
    public function setProfileCache($profileCache)
    {
        $this->profileCache = $profileCache;
    
        return $this;
    }

    /**
     * Get profileCache
     *
     * @return string 
     */
    public function getProfileCache()
    {
        return $this->profileCache;
    }

    /**
     * Set gallery
     *
     * @param string $gallery
     * @return XcConfig
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;
    
        return $this;
    }

    /**
     * Get gallery
     *
     * @return string 
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Set modules
     *
     * @param string $modules
     * @return XcConfig
     */
    public function setModules($modules)
    {
        $this->modules = $modules;
    
        return $this;
    }

    /**
     * Get modules
     *
     * @return string 
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Set wiadomosci
     *
     * @param integer $wiadomosci
     * @return XcConfig
     */
    public function setWiadomosci($wiadomosci)
    {
        $this->wiadomosci = $wiadomosci;
    
        return $this;
    }

    /**
     * Get wiadomosci
     *
     * @return integer 
     */
    public function getWiadomosci()
    {
        return $this->wiadomosci;
    }

    /**
     * Set liczbaOdwiedzin
     *
     * @param integer $liczbaOdwiedzin
     * @return XcConfig
     */
    public function setLiczbaOdwiedzin($liczbaOdwiedzin)
    {
        $this->liczbaOdwiedzin = $liczbaOdwiedzin;
    
        return $this;
    }

    /**
     * Get liczbaOdwiedzin
     *
     * @return integer 
     */
    public function getLiczbaOdwiedzin()
    {
        return $this->liczbaOdwiedzin;
    }

    /**
     * Set newsletterCount
     *
     * @param integer $newsletterCount
     * @return XcConfig
     */
    public function setNewsletterCount($newsletterCount)
    {
        $this->newsletterCount = $newsletterCount;
    
        return $this;
    }

    /**
     * Get newsletterCount
     *
     * @return integer 
     */
    public function getNewsletterCount()
    {
        return $this->newsletterCount;
    }

    /**
     * Set formularzProduktowy
     *
     * @param integer $formularzProduktowy
     * @return XcConfig
     */
    public function setFormularzProduktowy($formularzProduktowy)
    {
        $this->formularzProduktowy = $formularzProduktowy;
    
        return $this;
    }

    /**
     * Get formularzProduktowy
     *
     * @return integer 
     */
    public function getFormularzProduktowy()
    {
        return $this->formularzProduktowy;
    }

    /**
     * Set product
     *
     * @param string $product
     * @return XcConfig
     */
    public function setProduct($product)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return string 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set listing
     *
     * @param string $listing
     * @return XcConfig
     */
    public function setListing($listing)
    {
        $this->listing = $listing;
    
        return $this;
    }

    /**
     * Get listing
     *
     * @return string 
     */
    public function getListing()
    {
        return $this->listing;
    }

    /**
     * Set news
     *
     * @param string $news
     * @return XcConfig
     */
    public function setNews($news)
    {
        $this->news = $news;
    
        return $this;
    }

    /**
     * Get news
     *
     * @return string 
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Set boxes
     *
     * @param string $boxes
     * @return XcConfig
     */
    public function setBoxes($boxes)
    {
        $this->boxes = $boxes;
    
        return $this;
    }

    /**
     * Get boxes
     *
     * @return string 
     */
    public function getBoxes()
    {
        return $this->boxes;
    }

    /**
     * Set layout
     *
     * @param string $layout
     * @return XcConfig
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    
        return $this;
    }

    /**
     * Get layout
     *
     * @return string 
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Set wwwRedirect
     *
     * @param integer $wwwRedirect
     * @return XcConfig
     */
    public function setWwwRedirect($wwwRedirect)
    {
        $this->wwwRedirect = $wwwRedirect;
    
        return $this;
    }

    /**
     * Get wwwRedirect
     *
     * @return integer 
     */
    public function getWwwRedirect()
    {
        return $this->wwwRedirect;
    }

    /**
     * Set robotsTxt
     *
     * @param string $robotsTxt
     * @return XcConfig
     */
    public function setRobotsTxt($robotsTxt)
    {
        $this->robotsTxt = $robotsTxt;
    
        return $this;
    }

    /**
     * Get robotsTxt
     *
     * @return string 
     */
    public function getRobotsTxt()
    {
        return $this->robotsTxt;
    }

    /**
     * Set twojeZaplecze
     *
     * @param string $twojeZaplecze
     * @return XcConfig
     */
    public function setTwojeZaplecze($twojeZaplecze)
    {
        $this->twojeZaplecze = $twojeZaplecze;
    
        return $this;
    }

    /**
     * Get twojeZaplecze
     *
     * @return string 
     */
    public function getTwojeZaplecze()
    {
        return $this->twojeZaplecze;
    }
}
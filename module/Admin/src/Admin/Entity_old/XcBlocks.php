<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcBlocks
 *
 * @ORM\Table(name="xc_blocks")
 * @ORM\Entity
 */
class XcBlocks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_block", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idBlock;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_template", type="bigint", nullable=false)
     */
    private $idTemplate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_location", type="integer", nullable=false)
     */
    private $idLocation;

    /**
     * @var integer
     *
     * @ORM\Column(name="depth", type="integer", nullable=false)
     */
    private $depth;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=false)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=255, nullable=false)
     */
    private $class;

    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=false)
     */
    private $width;



    /**
     * Get idBlock
     *
     * @return integer 
     */
    public function getIdBlock()
    {
        return $this->idBlock;
    }

    /**
     * Set idTemplate
     *
     * @param integer $idTemplate
     * @return XcBlocks
     */
    public function setIdTemplate($idTemplate)
    {
        $this->idTemplate = $idTemplate;
    
        return $this;
    }

    /**
     * Get idTemplate
     *
     * @return integer 
     */
    public function getIdTemplate()
    {
        return $this->idTemplate;
    }

    /**
     * Set idLocation
     *
     * @param integer $idLocation
     * @return XcBlocks
     */
    public function setIdLocation($idLocation)
    {
        $this->idLocation = $idLocation;
    
        return $this;
    }

    /**
     * Get idLocation
     *
     * @return integer 
     */
    public function getIdLocation()
    {
        return $this->idLocation;
    }

    /**
     * Set depth
     *
     * @param integer $depth
     * @return XcBlocks
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
    
        return $this;
    }

    /**
     * Get depth
     *
     * @return integer 
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return XcBlocks
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return XcBlocks
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    
        return $this;
    }

    /**
     * Get template
     *
     * @return string 
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return XcBlocks
     */
    public function setClass($class)
    {
        $this->class = $class;
    
        return $this;
    }

    /**
     * Get class
     *
     * @return string 
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return XcBlocks
     */
    public function setWidth($width)
    {
        $this->width = $width;
    
        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }
}
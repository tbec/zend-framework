<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcBannersLang
 *
 * @ORM\Table(name="xc_banners_lang")
 * @ORM\Entity
 */
class XcBannersLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="bigint", nullable=false)
     */
    private $idLang;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_banner", type="bigint", nullable=false)
     */
    private $idBanner;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     * @return XcBannersLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;
    
        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer 
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set idBanner
     *
     * @param integer $idBanner
     * @return XcBannersLang
     */
    public function setIdBanner($idBanner)
    {
        $this->idBanner = $idBanner;
    
        return $this;
    }

    /**
     * Get idBanner
     *
     * @return integer 
     */
    public function getIdBanner()
    {
        return $this->idBanner;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return XcBannersLang
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return XcBannersLang
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
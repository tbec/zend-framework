<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcProducts
 *
 * @ORM\Table(name="xc_products")
 * @ORM\Entity
 */
class XcProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="import_id", type="bigint", nullable=false)
     */
    private $importId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_manufacturer", type="bigint", nullable=false)
     */
    private $idManufacturer;

    /**
     * @var integer
     *
     * @ORM\Column(name="availability", type="integer", nullable=false)
     */
    private $availability;

    /**
     * @var integer
     *
     * @ORM\Column(name="unit", type="integer", nullable=false)
     */
    private $unit;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=255, nullable=false)
     */
    private $symbol;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_category", type="bigint", nullable=false)
     */
    private $idCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_quantity", type="integer", nullable=false)
     */
    private $maxQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="promotion", type="integer", nullable=false)
     */
    private $promotion;

    /**
     * @var integer
     *
     * @ORM\Column(name="sold", type="integer", nullable=false)
     */
    private $sold;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity_hurt", type="integer", nullable=false)
     */
    private $quantityHurt;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255, nullable=false)
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="attributes", type="text", nullable=false)
     */
    private $attributes;



    /**
     * Get idProduct
     *
     * @return integer 
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set importId
     *
     * @param integer $importId
     * @return XcProducts
     */
    public function setImportId($importId)
    {
        $this->importId = $importId;
    
        return $this;
    }

    /**
     * Get importId
     *
     * @return integer 
     */
    public function getImportId()
    {
        return $this->importId;
    }

    /**
     * Set idManufacturer
     *
     * @param integer $idManufacturer
     * @return XcProducts
     */
    public function setIdManufacturer($idManufacturer)
    {
        $this->idManufacturer = $idManufacturer;
    
        return $this;
    }

    /**
     * Get idManufacturer
     *
     * @return integer 
     */
    public function getIdManufacturer()
    {
        return $this->idManufacturer;
    }

    /**
     * Set availability
     *
     * @param integer $availability
     * @return XcProducts
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    
        return $this;
    }

    /**
     * Get availability
     *
     * @return integer 
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Set unit
     *
     * @param integer $unit
     * @return XcProducts
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    
        return $this;
    }

    /**
     * Get unit
     *
     * @return integer 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     * @return XcProducts
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    
        return $this;
    }

    /**
     * Get symbol
     *
     * @return string 
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set idCategory
     *
     * @param integer $idCategory
     * @return XcProducts
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;
    
        return $this;
    }

    /**
     * Get idCategory
     *
     * @return integer 
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }

    /**
     * Set maxQuantity
     *
     * @param integer $maxQuantity
     * @return XcProducts
     */
    public function setMaxQuantity($maxQuantity)
    {
        $this->maxQuantity = $maxQuantity;
    
        return $this;
    }

    /**
     * Get maxQuantity
     *
     * @return integer 
     */
    public function getMaxQuantity()
    {
        return $this->maxQuantity;
    }

    /**
     * Set promotion
     *
     * @param integer $promotion
     * @return XcProducts
     */
    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;
    
        return $this;
    }

    /**
     * Get promotion
     *
     * @return integer 
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set sold
     *
     * @param integer $sold
     * @return XcProducts
     */
    public function setSold($sold)
    {
        $this->sold = $sold;
    
        return $this;
    }

    /**
     * Get sold
     *
     * @return integer 
     */
    public function getSold()
    {
        return $this->sold;
    }

    /**
     * Set quantityHurt
     *
     * @param integer $quantityHurt
     * @return XcProducts
     */
    public function setQuantityHurt($quantityHurt)
    {
        $this->quantityHurt = $quantityHurt;
    
        return $this;
    }

    /**
     * Get quantityHurt
     *
     * @return integer 
     */
    public function getQuantityHurt()
    {
        return $this->quantityHurt;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return XcProducts
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    
        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set attributes
     *
     * @param string $attributes
     * @return XcProducts
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    
        return $this;
    }

    /**
     * Get attributes
     *
     * @return string 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}
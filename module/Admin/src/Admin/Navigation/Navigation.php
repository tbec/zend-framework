<?php 
namespace Admin\Navigation;
 
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Navigation\Service\DefaultNavigationFactory;
 
class Navigation extends DefaultNavigationFactory
{
    protected function getPages(ServiceLocatorInterface $serviceLocator)
    {
        if (null === $this->pages) {
            $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
			
			$fetchMenu = $objectManager->getRepository('Application\Entity\XcPages')->findAll();
			
            $navigation = $config['navigation'][$this->getName()] = array();
            /*
			foreach($fetchMenu as $key=>$row)
            {
                $navigation[] = array(
                    'label' => $row->getTitle(),
                    'uri' => '/pages/details?id='.$row->getId(),
                );
            }
			*/
			
             
		
			$navigation[] = array (
                'label' => 'Account',
                'uri'   => '/admin/account'
            ); 
 
            $application = $serviceLocator->get('Application');
            $routeMatch  = $application->getMvcEvent()->getRouteMatch();
            $router      = $application->getMvcEvent()->getRouter();
            $pages       = $this->getPagesFromConfig($navigation);
 
            $this->pages = $this->injectComponents(
                $pages,
                $routeMatch,
                $router
            );
        }
        return $this->pages;
    }
}
<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcManufacturers
 *
 * @ORM\Table(name="xc_manufacturers")
 * @ORM\Entity
 */
class XcManufacturers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_producenta", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProducenta;

    /**
     * @var integer
     *
     * @ORM\Column(name="import_id", type="bigint", nullable=false)
     */
    private $importId;

    /**
     * @var integer
     *
     * @ORM\Column(name="kolejnosc", type="bigint", nullable=false)
     */
    private $kolejnosc;

    /**
     * @var string
     *
     * @ORM\Column(name="adres", type="string", length=255, nullable=false)
     */
    private $adres;

    /**
     * @var string
     *
     * @ORM\Column(name="kod", type="string", length=6, nullable=false)
     */
    private $kod;

    /**
     * @var string
     *
     * @ORM\Column(name="miasto", type="string", length=255, nullable=false)
     */
    private $miasto;

    /**
     * @var string
     *
     * @ORM\Column(name="telefon", type="string", length=10, nullable=false)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(name="nip", type="string", length=10, nullable=false)
     */
    private $nip;

    /**
     * @var string
     *
     * @ORM\Column(name="regon", type="string", length=10, nullable=false)
     */
    private $regon;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=10, nullable=false)
     */
    private $email;



    /**
     * Get idProducenta
     *
     * @return integer 
     */
    public function getIdProducenta()
    {
        return $this->idProducenta;
    }

    /**
     * Set importId
     *
     * @param integer $importId
     * @return XcManufacturers
     */
    public function setImportId($importId)
    {
        $this->importId = $importId;
    
        return $this;
    }

    /**
     * Get importId
     *
     * @return integer 
     */
    public function getImportId()
    {
        return $this->importId;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return XcManufacturers
     */
    public function setKolejnosc($kolejnosc)
    {
        $this->kolejnosc = $kolejnosc;
    
        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer 
     */
    public function getKolejnosc()
    {
        return $this->kolejnosc;
    }

    /**
     * Set adres
     *
     * @param string $adres
     * @return XcManufacturers
     */
    public function setAdres($adres)
    {
        $this->adres = $adres;
    
        return $this;
    }

    /**
     * Get adres
     *
     * @return string 
     */
    public function getAdres()
    {
        return $this->adres;
    }

    /**
     * Set kod
     *
     * @param string $kod
     * @return XcManufacturers
     */
    public function setKod($kod)
    {
        $this->kod = $kod;
    
        return $this;
    }

    /**
     * Get kod
     *
     * @return string 
     */
    public function getKod()
    {
        return $this->kod;
    }

    /**
     * Set miasto
     *
     * @param string $miasto
     * @return XcManufacturers
     */
    public function setMiasto($miasto)
    {
        $this->miasto = $miasto;
    
        return $this;
    }

    /**
     * Get miasto
     *
     * @return string 
     */
    public function getMiasto()
    {
        return $this->miasto;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     * @return XcManufacturers
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    
        return $this;
    }

    /**
     * Get telefon
     *
     * @return string 
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Set nip
     *
     * @param string $nip
     * @return XcManufacturers
     */
    public function setNip($nip)
    {
        $this->nip = $nip;
    
        return $this;
    }

    /**
     * Get nip
     *
     * @return string 
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set regon
     *
     * @param string $regon
     * @return XcManufacturers
     */
    public function setRegon($regon)
    {
        $this->regon = $regon;
    
        return $this;
    }

    /**
     * Get regon
     *
     * @return string 
     */
    public function getRegon()
    {
        return $this->regon;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return XcManufacturers
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
}
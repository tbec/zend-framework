<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;   
use Zend\InputFilter\InputFilterInterface; 

/**
 * Entity
 */
class Entity
{   
   public $inputFilter;

   public function get($field)
   {
       
		$method = 'get'.ucfirst($field);
		if($field=='' || !method_exists($this,$method)) return false;
		return $this->$method();
   }
   
	/**
	* Magic getter to expose protected properties.
	*
	* @param string $property
	* @return mixed
	*/
	public function __get($property)
	{
	return $this->$property;
	}

	/**
	* Magic setter to save protected properties.
	*
	* @param string $property
	* @param mixed $value
	*/
	public function __set($property, $value)
	{
	$this->$property = $value;
	}

	/**
	* Convert the object to an array.
	*
	* @return array
	*/
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}

	/**
	* Populate from an array.
	*
	* @param array $data
	*/
	public function populate(array $data = array(),$fields = array())
	{
            foreach($data as $property => $value) {
        	if($property=='password') 
        	{
    	    	if(empty($value)) $value=$fields[$property]['old'];
				else $value = sha1($value);
			}
			
            $method = 'set' . ucfirst($property);
		
            if(method_exists($this, $method) and !is_null($value)) {
                $this->$method($value);
            }
        }
	}
	
	
	    // Add content to this method:
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter($fields)
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
                
			foreach($fields as $k=>$f){
				unset($f['type']);
                $inputFilter->add($factory->createInput($f));
				
			}

           
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
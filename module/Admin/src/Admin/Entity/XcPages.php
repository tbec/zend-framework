<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcPages
 *
 * @ORM\Table(name="xc_pages")
 * @ORM\Entity
 */
class XcPages extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=100, nullable=false)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=false)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="menu", type="integer", nullable=false)
     */
    private $menu;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="text", nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text", nullable=false)
     */
    private $metaKeywords;

    /**
     * @var integer
     *
     * @ORM\Column(name="const", type="integer", nullable=false)
     */
    private $const;

    /**
     * @var integer
     *
     * @ORM\Column(name="depth", type="bigint", nullable=false)
     */
    private $depth;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent", type="bigint", nullable=false)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=500, nullable=false)
     */
    private $path;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="bigint", nullable=false)
     */
    private $views;

    /**
     * @var string
     *
     * @ORM\Column(name="target", type="string", length=255, nullable=false)
     */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=255, nullable=false)
     */
    private $alt;

    /**
     * @var string
     *
     * @ORM\Column(name="rel", type="string", length=255, nullable=false)
     */
    private $rel;

    /**
     * @var string
     *
     * @ORM\Column(name="external_link", type="string", length=255, nullable=false)
     */
    private $externalLink;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_off", type="integer", nullable=false)
     */
    private $isOff;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_gallery", type="integer", nullable=false)
     */
    private $countGallery;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_files", type="integer", nullable=false)
     */
    private $countFiles;

    /**
     * @var string
     *
     * @ORM\Column(name="thumb", type="string", length=255, nullable=false)
     */
    private $thumb;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return XcPages
     */
    public function setRoute($route)
    {
        $this->route = $route;
    
        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     * @return XcPages
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    
        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string 
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return XcPages
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return XcPages
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return XcPages
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set menu
     *
     * @param integer $menu
     * @return XcPages
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
    
        return $this;
    }

    /**
     * Get menu
     *
     * @return integer 
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return XcPages
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return XcPages
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return XcPages
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set const
     *
     * @param integer $const
     * @return XcPages
     */
    public function setConst($const)
    {
        $this->const = $const;
    
        return $this;
    }

    /**
     * Get const
     *
     * @return integer 
     */
    public function getConst()
    {
        return $this->const;
    }

    /**
     * Set depth
     *
     * @param integer $depth
     * @return XcPages
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
    
        return $this;
    }

    /**
     * Get depth
     *
     * @return integer 
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     * @return XcPages
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return integer 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return XcPages
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return XcPages
     */
    public function setViews($views)
    {
        $this->views = $views;
    
        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set target
     *
     * @param string $target
     * @return XcPages
     */
    public function setTarget($target)
    {
        $this->target = $target;
    
        return $this;
    }

    /**
     * Get target
     *
     * @return string 
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set alt
     *
     * @param string $alt
     * @return XcPages
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    
        return $this;
    }

    /**
     * Get alt
     *
     * @return string 
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set rel
     *
     * @param string $rel
     * @return XcPages
     */
    public function setRel($rel)
    {
        $this->rel = $rel;
    
        return $this;
    }

    /**
     * Get rel
     *
     * @return string 
     */
    public function getRel()
    {
        return $this->rel;
    }

    /**
     * Set externalLink
     *
     * @param string $externalLink
     * @return XcPages
     */
    public function setExternalLink($externalLink)
    {
        $this->externalLink = $externalLink;
    
        return $this;
    }

    /**
     * Get externalLink
     *
     * @return string 
     */
    public function getExternalLink()
    {
        return $this->externalLink;
    }

    /**
     * Set isOff
     *
     * @param integer $isOff
     * @return XcPages
     */
    public function setIsOff($isOff)
    {
        $this->isOff = $isOff;
    
        return $this;
    }

    /**
     * Get isOff
     *
     * @return integer 
     */
    public function getIsOff()
    {
        return $this->isOff;
    }

    /**
     * Set countGallery
     *
     * @param integer $countGallery
     * @return XcPages
     */
    public function setCountGallery($countGallery)
    {
        $this->countGallery = $countGallery;
    
        return $this;
    }

    /**
     * Get countGallery
     *
     * @return integer 
     */
    public function getCountGallery()
    {
        return $this->countGallery;
    }

    /**
     * Set countFiles
     *
     * @param integer $countFiles
     * @return XcPages
     */
    public function setCountFiles($countFiles)
    {
        $this->countFiles = $countFiles;
    
        return $this;
    }

    /**
     * Get countFiles
     *
     * @return integer 
     */
    public function getCountFiles()
    {
        return $this->countFiles;
    }

    /**
     * Set thumb
     *
     * @param string $thumb
     * @return XcPages
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;
    
        return $this;
    }

    /**
     * Get thumb
     *
     * @return string 
     */
    public function getThumb()
    {
        return $this->thumb;
    }
}
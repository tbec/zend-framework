<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class NewsController extends BaseController
{
        public $entity = 'XcNews';
    
	public function __construct()
	{
		$this->config = array(
			'primary'=> 'id',
			'fields'=>
			array(
				'id'=>array(
					'list'=>true, #show on grid list
					'name'=>'id',
					'label'=>'ID',
					'placeholder'=>'ID',
					'type'=>'text',
					'required'=>false,
					'disabled'=>1,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array()
				),
                               
                'isOff'=>array(
                            'list'=>true,
							'name'=>'isOff',
							'label'=>'Status',
							'placeholder'=>'1',
							'type'=>'switch',
                            'class'=>'',
                            'options' => array(
                                    'value_options' => array (0 => 'No', 1 => 'Yes' ) 
                            )
						   
				),
                            
                                'image'=>array(
					'list'=>true, #show on grid list
					'name'=>'image',
					'label'=>'Thumbnail',
					'placeholder'=>'ID',
					'type'=>'fileupload',
					'required'=>false,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array(),
                                        'options' => array(
                                                'uploadDir' => '/uploads/pages/images',
                                                'uploadSize' => 2000,
                                                'multi' => false,
                                                'uploadlimit'=>1,
                                                'thumbnails' => array(
                                                    array(96,96),
                                                    array(128,128),
                                                    array(300,300),
                                                    array(800,600)
                                                )
                                        )
				),
                               
				'title'=>array(
					'list'=>true, #show on grid list
					'name'=>'title',
					'label'=>'Title',
					'placeholder'=>'type something ...',
					'type'=>'text',
					'class'=>'',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 5,
								'max'      => 100,
							),
						),
					)
				),
				
				'content'=>array(
					'name'=>'content',
					'label'=>'Content',
					'placeholder'=>'type something ...',
					'type'=>'textarea',
					'class'=>'cm-editor',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 1,
								'max'      => 500,
							),
						),
					)
				),
			
				'position'=>array(
							'list'=>true, #show on grid list
							'name'=>'position',
							'label'=>'Position',
							'placeholder'=>'1',
							'type'=>'text'
						   
				),
				'views'=>array(
							'list'=>true, #show on grid list
							'name'=>'views',
							'label'=>'Views',
							'placeholder'=>'0',
							'required'=>false,
							'disabled'=>1,
							'type'=>'text',
							'filters'  => array(
							array('name' => 'Int')
							)
				),
			),
			'use'=>array(),
			'labels'=>array(
				'title'=>'News',
				'add'=>'Add new item',
				'edit'=>'Edit item',
				'delete'=>'Delete item'
			),
                        'modules'=>array('editor','dropkick','uploadify'),
                        'tabs'=>array('Default')
		);
		
		
		$this->addSeo();
	}
        
		
        protected function prepareSelect($entity,$key,$value)
        {
            $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $repo = $objectManager->getRepository('Admin\Entity\\'.$entity);
           
            $res = $repo->createQueryBuilder('p')
            ->select('p.'.$key.' as id,p.'.$value.' as value')
            ->where('p.parent = 0')
            ->orderBy('p.parent', 'ASC')
            ->getQuery()
            ->getArrayResult();
            
            foreach($res as $k=>$r)
                $rr[$r['id']]=$r['value'];
            
            
            
            return $rr;
        }
	
	protected function prepareData($data)
	{
                if(!isset($data['isOff'])) $data['isOff'] = 0;
		$default = array(
		'content'=>'',
		'menu'=>'',
		'route'=>'',
		'subtitle'=>'',
		'metaTitle'=>'',
		'const'=>'',
		'depth'=>0,
		'parent'=>0,
		'path'=>'',
		'views'=>0,
		'target'=>'_self',
		'alt'=>'',
		'rel'=>'',
		'thumb'=>'',
		'externalLink'=>'',
		'isOff'=>0,
		'countGallery'=>'',
		'countFiles'=>'',
		'views'=>0
		);
                
		return array_merge($default,$data);
	}
}
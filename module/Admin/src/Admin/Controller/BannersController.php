<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class BannersController extends BaseController
{
        public $entity = 'XcBanners';
    
	public function __construct()
	{
		$this->config = array(
			'primary'=> 'idImage',
			'fields'=>
			array(
				'idImage'=>array(
					'list'=>true, #show on grid list
					'name'=>'idImage',
					'label'=>'ID',
					'placeholder'=>'ID',
					'type'=>'text',
					'required'=>false,
					'disabled'=>1,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array()
				),
                                
                                'image'=>array(
					'list'=>true, #show on grid list
					'name'=>'image',
					'label'=>'Thumbnail',
					'placeholder'=>'ID',
					'type'=>'image',
					'required'=>false,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array(),
                                        'options' => array(
                                                'uploadDir' => '/uploads/pages/images',
                                                'uploadSize' => 2000,
                                                'multi' => false,
                                                'uploadlimit'=>1,
                                                'thumbnails' => array(
                                                    array(96,96),
                                                    array(128,128),
                                                    array(300,300),
                                                    array(800,600)
                                                )
                                        )
				),
                            
                                'title'=>array(
					'list'=>true, #show on grid list
					'name'=>'title',
					'label'=>'Title',
					'placeholder'=>'type something ...',
					'type'=>'text',
					'class'=>'',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 5,
								'max'      => 100,
							),
						),
					)
				),
                            
                                'description'=>array(
					'name'=>'description',
					'label'=>'Content',
					'placeholder'=>'type something ...',
					'type'=>'textarea',
					'class'=>'',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 1,
								'max'      => 500,
							),
						),
					)
				),
                            
                                'position'=>array(
							'list'=>true, #show on grid list
							'name'=>'position',
							'label'=>'Position',
							'placeholder'=>'1',
							'type'=>'text'
						   
				),
                            
				'position'=>array(
							'list'=>true, #show on grid list
							'name'=>'position',
							'label'=>'Position',
							'placeholder'=>'1',
							'type'=>'text'
						   
				),
				'views'=>array(
							'list'=>true, #show on grid list
							'name'=>'views',
							'label'=>'Views',
							'placeholder'=>'0',
							'required'=>false,
							'disabled'=>1,
							'type'=>'text',
							'filters'  => array(
							array('name' => 'Int')
							)
				),
                            
                                'status'=>array(
                                                        'list'=>true,
							'name'=>'status',
							'label'=>'Status',
							'placeholder'=>'1',
							'type'=>'select',
                                                        'options' => array(
                                                                'value_options' => array (0 => 'No', 1 => 'Yes' ) 
                                                        )
						   
				),
			),
			'use'=>array(),
			'labels'=>array(
				'title'=>'News',
				'add'=>'Add new item',
				'edit'=>'Edit item',
				'delete'=>'Delete item'
			),
                        'modules'=>array('dropkick')
		);
		
	}
        
        protected function prepareSelect($entity,$key,$value)
        {
            $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $repo = $objectManager->getRepository('Admin\Entity\\'.$entity);
           
            $res = $repo->createQueryBuilder('p')
            ->select('p.'.$key.' as id,p.'.$value.' as value')
            ->where('p.parent = 0')
            ->orderBy('p.parent', 'ASC')
            ->getQuery()
            ->getArrayResult();
            
            foreach($res as $k=>$r)
                $rr[$r['id']]=$r['value'];
            
            
            
            return $rr;
        }
	
	protected function prepareData($data)
	{
		$default = array(
		'content'=>'',
		'menu'=>'',
		'route'=>'',
		'subtitle'=>'',
		'metaTitle'=>'',
		'const'=>'',
		'depth'=>0,
		'parent'=>0,
		'path'=>'',
		'views'=>0,
		'target'=>'_self',
		'alt'=>'',
		'rel'=>'',
		'thumb'=>'',
		'externalLink'=>'',
		'isOff'=>'',
		'countGallery'=>'',
		'countFiles'=>'',
		'views'=>0
		);
	
	
		return array_merge($data,$default);
	}
}
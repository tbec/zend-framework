<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;
use PDO;
use Zend\Validator\File\Size as Size;
use Admin\Forms\UploadForm;

class UploadController extends ActionController
{
        public $db = null;
    
        public function __construct()
	{
            $config = array(
               'host'=>'localhost',
               'username'=>'root',
               'password'=>'',
               'dbname'=>'zend_test',
               'init'=>'SET NAMES utf8'
            );
            $this->db = new PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'], $config['username'], $config['password'],array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,PDO::MYSQL_ATTR_INIT_COMMAND => $config['init']));
            $this->db-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        
        public function indexAction()
        {
            $this->view_data['images'] = $this->db->query('SELECT * FROM xc_images')->fetchAll();     
            return $this->_render();
        }
        
        public function uploadAction()
        {
                $form = new UploadForm();
                
                   $request = $this->getRequest();
                   if ($request->isPost()) {
                        $errors = array();
                        $data = array();
                        $msg = 'Upload error';
                        $title = 'Error';
                        $type =0;

                        $nonFile = $request->getPost()->toArray();
                        $File    = $this->params()->fromFiles('file');
                        $data = array_merge(
                             $nonFile,
                             array('file'=> $File['name'])
                         );
                        
                        $form->setInputFilter($form->getInputFilter());
                        
                        $form->setData($data);
                        
                        $adapter = new \Zend\File\Transfer\Adapter\Http();
                        $path = pathinfo($File['name']);
                        $ext = strtolower($path['extension']);
                        $adapter->addFilter('Rename', dirname(__DIR__).'/../../../../httpdocs/uploads/'.date('U').'.'.$ext);
                        $adapter->setValidators(array(new Size(array('max'=>2000000))), $File['name']);
                        $adapter->addValidator('Extension', false, array('jpg', 'png','gif'));
                        if ($form->isValid()) {
                                if ($adapter->isValid()) {

                                    $adapter->setDestination(dirname(__DIR__).'/../../../../httpdocs/uploads');
                                    if ($adapter->receive($File['name'])) {
                                         $adapter->setOptions(array('useByteString' => false));
                                         $size = $adapter->getFileSize();
                                       
                                         $thumbnailer = $this->getServiceLocator()->get('WebinoImageThumb');
                                         $thumb = $thumbnailer->create($adapter->getFileName(), $options = array());
                                         
                                         $thumb->resize(116, 116);
                                         $name = basename($adapter->getFileName());
                                         $path = dirname(__DIR__).'/../../../../httpdocs/uploads/116_116_'.$name;
                                         $thumb->save($path);
                                         
                                        list($alt,$path,$name,$type,$size) = array(
                                            $File['name'],
                                            '',
                                            $name,
                                            'image',
                                            $size
                                        );

                                        $this->db->query('INSERT INTO xc_images(id_category,id_object,alt,path,name,type,size) 
                                        VALUES(
                                        1,
                                        1,
                                        "'.$alt.'",
                                        "'.$path.'",
                                        "'.$name.'",
                                        "'.$type.'",
                                        "'.$size.'"
                                        )'); 
                                        
                                        $data = array(
                                            array('path'=>$path,'name'=>$name,'alt'=>$alt,'type'=>$type,'size'=>$size)
                                        );
                                        $type=1;
                                        $title = 'Success';
                                        $msg = 'Image uploaded';
                                    }
                                   
                                }
                                else {
                                    $dataError = $adapter->getMessages();
                                    foreach($dataError as $key=>$row)
                                    $errors[$key] = $row;
                                    
                                    $form->setMessages(array('fileupload'=>$error ));
                                   
                                }
                        }else {
                            $dataError = $form->getMessages();
                            foreach($dataError as $key=>$row)
                            $errors[$key] = $row;
                        }
                        
                        $output = array(
                            'type' => $type,
                            'msg' => $msg,
                            'data'=>$data,
                            'errors'=>$errors
                        );
                        
                        echo json_encode($output);
                        exit();
                    }

                   
                
                /*
               

                if (!$adapter->isValid()){
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach($dataError as $key=>$row)
                    {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('fileupload'=>$error ));
                } else {
                    $adapter->setDestination(dirname(__DIR__).'/assets');
                    if ($adapter->receive($File['name'])) {
                        $profile->exchangeArray($form->getData());
                        echo 'Profile Name '.$profile->profilename.' upload '.$profile->fileupload;
                    }
                } 
            } 
                 */
                
                $this->view_data['form'] = $form;
                return $this->_render();
            
        }
}
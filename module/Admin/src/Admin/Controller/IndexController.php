<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\ActionController;
use Zend\View\Model\ViewModel;
class IndexController extends ActionController
{
    public function indexAction()
    {

	   $auth = $this->getAuth();
	   if (!$auth->hasIdentity()) return $this->redirect()->toUrl('account/login');
       $request = $this->getRequest();
       $response = $this->getResponse();
      
       echo $request->isXmlHttpRequest(); //check AJAX request
       echo $request->isPost(); //check Post
	   
       $this->view_data['breadcrumbs'] = 'Home &raquo; Pages';
       return $this->_render();       
    }
    
    public function ajaxAction(){
        
        #using json strategy
        return new JsonModel(array(
        'success'=>true
        )); 
        
        #simple json output
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode(array('success'=>1)));
        return $response;
    }
}

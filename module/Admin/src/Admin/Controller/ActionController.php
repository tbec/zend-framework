<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionController
 *
 * @author Tomasz
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use SmartyModule\View\Renderer\SmartyRenderer as SmartyRenderer;

class ActionController extends AbstractActionController{
    
    public $em = null;
    public $view_data = null;
    public $scripts = null;
    public $styles = null; 
    
	public function _render($custom='',$terminal=false){
		
		$helper = $this->getServiceLocator()->get('viewhelpermanager');
		
		 $base = '/admin';
		
		 $helper->get('HeadScript')
		->prependFile("$base/../js/jquery.min.js", "text/javascript")
		->appendFile("$base/../js/jquery_ui/jquery-ui-1.10.2.custom.js", "text/javascript")
		->appendFile("$base/public/js/jquery.tipsy.js", "text/javascript")
                ->appendFile("$base/public/js/bootstrap-modal.js", "text/javascript")
		->appendFile("$base/public/js/main.js", "text/javascript")
		->appendFile("$base/public/js/api/notification.js", "text/javascript");
                 
                 if($this->scripts) foreach($this->scripts as $s)  $helper->get('HeadScript')->appendFile($base.$s, "text/javascript");
                 
		$helper->get('HeadLink')
		->appendStylesheet("$base/public/css/reset.css")
		->appendStylesheet("$base/public/css/main.css")
                ->appendStylesheet("$base/public/css/typography.css")
                ->appendStylesheet("$base/public/css/tipsy.css")
                ->appendStylesheet("$base/public/css/jquery.ui.all.css")
                ->appendStylesheet("$base/public/css/bootstrap.css")
                ->appendStylesheet("$base/public/js/jq_tables/demo_table_jui.css");
                
                if($this->styles) foreach($this->styles as $s)  $helper->get('HeadLink')->appendStylesheet($base.$s);
                 
		
		$auth = $this->getAuth();
		if ($auth->hasIdentity())  $this->view_data['user'] = $auth->getIdentity();
		$route = explode('\\',$this->getEvent()->getRouteMatch()->getParam('controller'));
		
		$this->view_data['controller']=strtolower($route[2]);
		if($this->view_data) foreach($this->view_data as $k=>$v)
		$this->layout()->setVariable($k,$v);
		$view = new ViewModel($this->view_data);
		if($custom) $view->setTemplate($custom);
              
                if($terminal) 
                {
                    $view->setTerminal(true);
                    $s = $this->getServiceLocator()->get('SmartyRenderer');
                    $view = $s->render($view);
                }
                
            
		return $view;
	}
	
	public function getAuth()
	{
	 return $this->getServiceLocator()->get('doctrine.authenticationservice.orm_default');
	}
	
    public function __construct(){
		
    }
	
	public function _json($data)
	{
		 #simple json output
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($data));
        return $response;
	}
}

?>

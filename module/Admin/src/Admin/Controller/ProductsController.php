<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class ProductsController extends BaseController
{
        public $entity = 'XcProducts';
    
	public function __construct()
	{
		$this->config = array(
			'primary'=> 'idProduct',
			'fields'=>
			array(
				'idProduct'=>array(
					'list'=>true, #show on grid list
					'name'=>'idProduct',
					'label'=>'ID',
					'placeholder'=>'ID',
					'type'=>'text',
					'required'=>false,
					'disabled'=>1,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array()
				),
                                'is_off'=>array(
							'name'=>'isOff',
							'label'=>'Disable product',
							'placeholder'=>'1',
							'type'=>'radio',
                                                        'options' => array(
                                                                'value_options' => array (0 => 'No', 1 => 'Yes' ) 
                                                        )
						   
				),
                            
                                
                                
                               
				'title'=>array(
					'list'=>true, #show on grid list
					'name'=>'title',
					'label'=>'Title',
					'placeholder'=>'type something ...',
					'type'=>'text',
					'class'=>'',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 5,
								'max'      => 100,
							),
						),
					)
				),
				
				'content'=>array(
					'name'=>'content',
					'label'=>'Description',
					'placeholder'=>'type something ...',
					'type'=>'textarea',
					'class'=>'',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 1,
								'max'      => 500,
							),
						),
					)
				),
				
				'metaTitle'=>array(
							'name'=>'metaTitle',
							'label'=>'META title',
							'placeholder'=>'',
							'required'=>false,
							'type'=>'text'
						   
				),
				'metaDescription'=>array(
							'name'=>'metaTescription',
							'label'=>'META description',
							'placeholder'=>'',
							'required'=>false,
							'type'=>'text'
						   
				),
				'metaKeywords'=>array(
							'name'=>'metaDescription',
							'label'=>'META keywords',
							'placeholder'=>'',
							'required'=>false,
							'type'=>'text'
						   
				),
			
				'maxQuantity'=>array(
							'list'=>true, #show on grid list
							'name'=>'maxQuantity',
							'label'=>'In stock',
							'placeholder'=>'1',
							'type'=>'text'
						   
				),
				'idCategory'=>array(
							'list'=>true, #show on grid list
							'name'=>'idCategory',
							'label'=>'Category id',
							'placeholder'=>'0',
							'required'=>false,
							'disabled'=>1,
							'type'=>'text',
							'filters'  => array(
							array('name' => 'Int')
							)
				),
			),
			'use'=>array(),
			'labels'=>array(
				'title'=>'Products',
				'add'=>'Add new product',
				'edit'=>'Edit product',
				'delete'=>'Delete product'
			)
		);
		
	}
        
        protected function prepareSelect($entity,$key,$value)
        {
            $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $repo = $objectManager->getRepository('Admin\Entity\\'.$entity);
           
            $res = $repo->createQueryBuilder('p')
            ->select('p.'.$key.' as id,p.'.$value.' as value')
            ->where('p.parent = 0')
            ->orderBy('p.parent', 'ASC')
            ->getQuery()
            ->getArrayResult();
            
            foreach($res as $k=>$r)
                $rr[$r['id']]=$r['value'];
            
            
            
            return $rr;
        }
	
	protected function prepareData($data)
	{
		$default = array(
		'content'=>'',
		'menu'=>'',
		'route'=>'',
		'subtitle'=>'',
		'metaTitle'=>'',
		'const'=>'',
		'depth'=>0,
		'parent'=>0,
		'path'=>'',
		'views'=>0,
		'target'=>'_self',
		'alt'=>'',
		'rel'=>'',
		'thumb'=>'',
		'externalLink'=>'',
		'isOff'=>'',
		'countGallery'=>'',
		'countFiles'=>'',
		'views'=>0
		);
	
	
		return array_merge($data,$default);
	}
}
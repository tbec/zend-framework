<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class CategoriesController extends BaseController
{
        public $entity = 'XcCategories';
    
	public function __construct()
	{
		$this->config = array(
			'primary'=> 'id_category',
			'fields'=>
			array(
				'id_category'=>array(
					'list'=>true, #show on grid list
					'name'=>'idCategory',
					'label'=>'ID',
					'placeholder'=>'ID',
					'type'=>'text',
					'required'=>false,
					'disabled'=>1,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array()
				),
                                'is_off'=>array(
							'name'=>'isOff',
							'label'=>'Disable page',
							'placeholder'=>'1',
							'type'=>'radio',
                                                        'options' => array(
                                                                'value_options' => array (0 => 'No', 1 => 'Yes' ) 
                                                        )
						   
				),
                            
                                
                                'parent'=>array(
                                            		'name'=>'parent',
							'label'=>'Select page',
							'type'=>'select',
                                                        'options' => array(
                                                                'empty_option' => 'Please select an parent',
                                                                'value_query' => array(
                                                                    'entity'=>'XcCategories',
                                                                    'key'=>'id_category',
                                                                    'value'=>'title'
                                                                ) 
                                                        )
						   
				),
                            
				'name'=>array(
					'list'=>true, #show on grid list
					'name'=>'name',
					'label'=>'Name',
					'placeholder'=>'type something ...',
					'type'=>'text',
					'class'=>'',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 5,
								'max'      => 100,
							),
						),
					)
				),
				
				
				'position'=>array(
							'list'=>true, #show on grid list
							'name'=>'position',
							'label'=>'Position',
							'placeholder'=>'1',
							'type'=>'text'
						   
				),
			),
			'use'=>array(),
			'labels'=>array(
				'title'=>'Categories',
				'add'=>'Add new category',
				'edit'=>'Edit category',
				'delete'=>'Delete category'
			)
		);
		
	}
        
        protected function prepareSelect($entity,$key,$value)
        {
            $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $repo = $objectManager->getRepository('Admin\Entity\\'.$entity);
           
            $res = $repo->createQueryBuilder('p')
            ->select('p.'.$key.' as id,p.'.$value.' as value')
            ->where('p.parent = 0')
            ->orderBy('p.parent', 'ASC')
            ->getQuery()
            ->getArrayResult();
            
            foreach($res as $k=>$r)
                $rr[$r['id']]=$r['value'];
            
            
            
            return $rr;
        }
	
	protected function prepareData($data)
	{
		$default = array(
		'route'=>'',
		'name'=>'',
		'metaTitle'=>'',
                'metaKeyword'=>'',
                'metaDesctiption'=>'',
		'depth'=>0,
		'parent'=>0,
		'path'=>'',
		'active'=>1,
		'thumbnail'=>'',
		'isOff'=>''
		);
	
	
		return array_merge($data,$default);
	}
}
<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\ActionController;
use Zend\View\Model\ViewModel;

class ErrorController extends ActionController
{
    public function indexAction()
    {  
		
		return new ViewModel(
		
		);
    }
	
	public function detailsAction()
    {  
		
		
		 $objectManager = $this
        ->getServiceLocator()
        ->get('Doctrine\ORM\EntityManager');

		$res = $objectManager->getRepository('Application\Entity\Pages')->find(1);
		$page = $res->get('page');
		$page = array(
		'title'=>$page->get('title'),
		'description'=>$page->get('description')
		);
		
		return new ViewModel($page);
	}
}
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionController
 *
 * @author Tomasz
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Form\Form as Form;
use Zend\Paginator\Paginator;
use Zend\Validator\File\Size as Size;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineObject;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use DoctrineORMModule\Form\Annotation\AnnotationBuilder as AnnotationBuilder;
use PDO;

class BaseController extends ActionController{
    
    public function _construct()
    {
        $this->helper = $this->getServiceLocator()->get('viewhelpermanager');
    }
	
    public $entity = null;
    public $config =  array();
	
    public $em = null;
    public $view_data = array();
    
		protected function generateForm()
		{
			$form = new Form();
			$form->setAttribute('method', 'post');
			$form->setAttribute('id', 'edit');
			$form->setAttribute('class', 'edit axform');
			$form->setAttribute('action', '');
			$form->setAttribute('enctype','multipart/form-data');	
			foreach($this->config['fields'] as $k=>&$f)
			{
                                if(isset($f['options']['value_query']))
                                {
                                    $fn = $f['options']['value_query'];
                                    $f['options']['value_options'] = $this->prepareSelect($fn['entity'],$fn['key'],$fn['value']);
                                }
                                if(!isset($f['class'])) $f['class'] = '';
				if(!isset($f['disabled'])) {
					$form->add(array(
						'name' => $k,
						'attributes' => array(
                                                        'class'=>$f['class'],
							'type' => $f['type'],
							'id'=>$k
						),
						'options' => array(
							'label'=>$f['label']
						)
					));
				}
			}
			
			$form->add(array(
				'name' => 'submit',
				'attributes' => array(
					'type' => 'submit',
					'value' => 'Go',
					'id' => 'submitbutton',
				),
			));
			
			
			return $form;
		}
	
        protected function displayableFields()
        {
            $fields = array();

            $metadata = $repo = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->getClassMetadata('Admin\Entity\\'.$this->entity);
            foreach ($metadata->getFieldNames() as $fieldName) {
                // Exclude association fields
                if ($metadata->hasAssociation($fieldName)) {
                    continue;
                }

                // Put identifiers on top of the list
                if ($metadata->isIdentifier($fieldName)) {
                    array_unshift($fields, $fieldName);
                } else {
                    array_push($fields, $fieldName);
                }
            }

            return $fields;
        }


        protected function requestedSorting()
        {
                $sorting = array();
                
                
                $sorting[0] = trim(strtolower($this->params()->fromQuery('sort', null)));
                $sorting[1] = ($this->params()->fromQuery('ord',null)=='desc')?'desc':'asc';
                
                if($sorting[0]===null || !in_array($sorting[0],$this->displayableFields())) return false;
                $this->view_data['sort'] = $sorting[0];
                $this->view_data['ord'] = $sorting[1];
                return $sorting;
        }

        public function initModules()
        {
            if(isset($this->config['modules'])) foreach($this->config['modules'] as $m)
            {
                switch($m)
                {
                    case 'dropkick':
                    $this->scripts[] = '/public/js/dropkick/dropkick.js';
                    $this->styles[] = '/public/js/dropkick/dropkick.css';
                    break;
                
                    case 'uploadify':
                    $this->scripts[] = '/public/uploadify/swfobject.js';
                    $this->scripts[]= '/public/uploadify/jquery.uploadify.v2.1.4.min.js';
                    $this->styles[] ='/public/uploadify/uploadify.css';
                    break;
                
                    case 'editor':
                    $this->scripts[] = '/public/js/cl_editor/jquery.cleditor.min.js';
                    $this->styles[] = '/public/js/cl_editor/jquery.cleditor.css';
                    break;
                }
            }
        }
        
        function convertImage($originalImage, $outputImage,$output='jpg', $quality=100)
        {
            // jpg, png, gif or bmp?
            $exploded = explode('.',$originalImage);
            $ext = $exploded[count($exploded) - 1]; 

            if (preg_match('/jpg|jpeg/i',$ext))
                $imageTmp=imagecreatefromjpeg($originalImage);
            else if (preg_match('/png/i',$ext))
                $imageTmp=imagecreatefrompng($originalImage);
            else if (preg_match('/gif/i',$ext))
                $imageTmp=imagecreatefromgif($originalImage);
            else if (preg_match('/bmp/i',$ext))
                $imageTmp=imagecreatefrombmp($originalImage);
            else
                return 0;

            switch($output)
            {
                case 'gif': imagegif($imageTmp, $outputImage); break;
                case 'png': $quality = (int)($quality/100 * 9);  imagepng($imageTmp, $outputImage, $quality); break;
                default: imagejpeg($imageTmp, $outputImage, $quality);
            }
            
            imagedestroy($imageTmp);

            return 1;
        }
        
        function upload($object_id,$files,$obj)
        {
            $out = array();
            foreach($this->config['fields'] as $k=>$f)
            {
                if($f['type']=='file'){
                        if(isset($files[$k]) && $files[$k]!='') {
                        $file = $files[$k];
                        $adapter = new \Zend\File\Transfer\Adapter\Http();
                        $path = pathinfo($file['name']);
                        $ext = strtolower($path['extension']);
                        $o = $f['options'];
                        $category_id = (int)$o['uploadCategoryId'];
                        $multi = (isset($o['multi']) && $o['multi'])?true:false;
                        $oext = (isset($o['uploadExtension']))?$o['uploadExtension']:'jpg';
                        $name = isset($o['uploadNamePattern'])?str_replace('[ID]',$object_id,$o['uploadNamePattern']):date('U');
                        $dir = dirname(__DIR__).'/../../../../httpdocs/uploads/'.$o['uploadDir'].'/';
                        if(!is_dir($dir)) mkdir ($dir, 777, true);

                        $adapter->addFilter('Rename', array('target'=>$dir.$name.'.'.$ext,'overwrite' => true));
                        $adapter->setValidators(array(new Size(array('max'=>$o['uploadSize']))), $file['name']);
                        $adapter->addValidator('Extension', false, $o['uploadExtensions']);
                        $adapter->setOptions(array('useByteString' => false));

                        if($adapter->isValid())
                        {
                           if ($adapter->receive($file['name'])) {

                                $size = $adapter->getFileSize();
                                $fname =$dir.$name.'.'.$oext;
                                $this->convertImage($adapter->getFileName(),$fname,$oext);

                                $thumbnailer = $this->getServiceLocator()->get('WebinoImageThumb');
                                $thumb = $thumbnailer->create($fname, $options = array());
                                $name = basename($fname);

                                foreach($o['thumbnails'] as $t)
                                {
                                    $thumb->resize($t[0], $t[1]);
                                    $path = $dir.$t[0].'_'.$t[1].'_'.$name;
                                    $thumb->save($path);
                                }

                               list($alt,$path,$name,$type,$size) = array(
                                   $file['name'],
                                   '',
                                   $name,
                                   'image',
                                   $size
                               );

                               $config = array(
                                    'host'=>'localhost',
                                    'username'=>'root',
                                    'password'=>'',
                                    'dbname'=>'zend_test',
                                    'init'=>'SET NAMES utf8'
                                 );
                                 $this->db = new PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'], $config['username'], $config['password'],array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,PDO::MYSQL_ATTR_INIT_COMMAND => $config['init']));
                                 $this->db-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $where='';
                                $mode = 'INSERT INTO ';
                                if(!$multi)
                                {
                                   $r = $this->db->query('SELECT COUNT(*) as id FROM xc_images WHERE id_object='.$object_id.' AND id_category='.$category_id.' LIMIT 1')->fetch();
                                   if($r['id']) { $mode = 'UPDATE'; $where=' WHERE id='.$r['id']; }
                                }

                               $this->db->exec($mode.' xc_images
                               SET 
                               id_category = '.$category_id.' ,
                               id_object = '.$object_id.',
                               alt = "'.$alt.'",
                               path  = "'.$path.'" ,
                               name  = "'.$name.'",
                               type  = "'.$type.'",
                               size = "'.$size.'" '.$where); 

                               $data = array(
                                   array('path'=>$path,'name'=>$name,'alt'=>$alt,'type'=>$type,'size'=>$size)
                               );

                               $out[$k] = $o['uploadDir'].'/[size]_'.$name;
                               $out['images'][$k] = '/uploads/'.str_replace('[size]','128_128',$o['uploadDir'].'/[size]_'.$name).'?t='.date('U');
                           }
                        }
                        else
                        {
                         var_dump($adapter->getMessages());   
                        }

                        }
                        else {
                            $out[$k] = $this->config['fields'][$k]['old'];
                            $out['images'][$k] = '/uploads/'.str_replace('[size]','128_128',$this->config['fields'][$k]['old']).'?t='.date('U');
                        }
                    }
                }
          
            return $out;
        }
    
	public function indexAction(){
		
		$request = $this->getRequest();
               
             
		$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		if($request->isXmlHttpRequest())
		{
			$iTotal=50;
			$iFilteredTotal=100;
			
			$res = $objectManager->getRepository('Admin\Entity\\'.$this->entity)->findAll();
			$output = array(
				"sEcho" => intval($_GET['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => $this->prepare_data($res)
			);
			
			echo json_encode($output);
			exit();
		}
                
                $this->displayableFields();
                $sorting = $this->requestedSorting();
				$this->view_data = array_merge($this->view_data,$this->config);
				$repo = $objectManager->getRepository('Admin\Entity\\'.$this->entity);
                
				
                $p =  $repo->createQueryBuilder('x');
				if(method_exists($this, 'before_list')) $this->before_list($p,$sorting); //Before list hook
                if($sorting) $p->addOrderBy('x.'.$sorting[0],$sorting[1]);
                
                $adapter = new DoctrineAdapter(new ORMPaginator($p));
                $paginator = new Paginator($adapter);
                $paginator->setDefaultItemCountPerPage(10);
                $page = (int)$this->params()->fromQuery('page');
                if($page) $paginator->setCurrentPageNumber($page);
                
                $this->initModules();
				
				if(method_exists($this, 'after_list')) $this->after_list($paginator); //After list hook
				$this->view_data['labels']= $this->config['labels'];
				$this->view_data['paginator'] = $paginator;
                
                $this->styles[]='/public/js/jq_tables/demo_table_jui.css';
				$this->scripts[]='/public/js/jq_tables/jquery.dataTables.js';
                
				return $this->_render('admin/abstract/index.tpl');
	}
	
	public function addAction(){
		$this->config['fields'][$this->config['primary']]['hidden'] = 1;
		
		$request = $this->getRequest();
		$form = $this->generateForm();
		$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		
		if ($request->isPost()) {
            $ent = 'Admin\Entity\\'.$this->entity;
            $product = new $ent();
       
            $form->setInputFilter($product->getInputFilter($this->config['fields']));
            $data = $this->prepareData($request->getPost()->getArrayCopy());
            $form->setData($data);
			
            $errors = '';
                if ($form->isValid()) {
                    $product->populate($data);
                    
                    $objectManager->persist($product);
                    $objectManager->flush();

                    list($type,$title,$msg,$data,$errors) = array(1,'Success','Added','','');
                }
                else {
                    foreach($form->getMessages() as $k=>$m)
                    $errors[$k] = array_shift($m);
                    
                    list($type,$title,$msg,$data,$errors) = array(0,'Error','Incorrect data','',$errors);	
                }
                
                
            }
		$this->view_data = array_merge($this->view_data,$this->config);
		$this->view_data['form'] = $form;
                
                
                if($request->isXmlHttpRequest())
                {
                    $output = array(
                            'type' => $type,
                            'msg' => $msg,
                            'data'=>$data,
                            'errors'=>$errors
                    );

                    echo json_encode($output);
                    exit();
                }
                
               
                $this->initModules();
                $this->scripts[]='/public/js/jquery.form.js';
		$this->scripts[]='/public/js/edit.js';
                
		return $this->_render('admin/abstract/edit.tpl');
	}
        
        
	public function editAction(){
            
		$id = (int)$this->params()->fromQuery('id');
	
		$request = $this->getRequest();
		$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		
		$form = $this->generateForm();
		//$form->setBindOnValidate(false);
        $repo = $objectManager->getRepository('Admin\Entity\\'.$this->entity); 
		$res = $repo->find($id);
                
                foreach($this->config['fields'] as $k=>$f)
                {
                    if($f['type']=='file' || $f['type']=='password') $this->config['fields'][$k]['old'] = $res->get($k);
                }
                            
		$form->setHydrator(new DoctrineHydrator($objectManager,'Admin\Entity\\'.$this->entity));
                $form->bind($res);
                
		if ($request->isPost()) {
                        $errors = array();
                        $data = $this->prepareData($request->getPost()->getArrayCopy());
                        $files = $this->params()->fromFiles();
                        foreach($files as $k=>$f) $data[$k]=$f['name']; 
                        
                        $form->setInputFilter($res->getInputFilter($this->config['fields']));
                        $form->setData($data);
			if ($form->isValid()) {
				
				if(method_exists($this, 'before_edit')) $this->before_edit($data); //Before update hook
				
								
                                $r = $this->upload($id,$files,$res);
                                $data = array_merge($data,$r);

								
                                $res->populate($data,$this->config['fields']);
                               
				$objectManager->persist($res);
				$objectManager->flush();
                
                if(method_exists($this, 'after_edit')) $this->after_edit($data); //After update hook
				                
                               list($type,$title,$msg,$data,$errors) = array(1,'Success','Saved',$data,'');
                        }
                        else {
                        foreach($form->getMessages() as $k=>$m)
                        $errors[$k] = array_shift($m);

                        list($type,$title,$msg,$data,$errors) = array(0,'Error','Incorrect data','',$errors);	
                        }
		}
                
		$this->view_data = array_merge($this->view_data,$this->config);

		
		$this->view_data['form'] = $form;
		$this->view_data['item'] = $res;
                
                
                 if($request->isXmlHttpRequest())
                {
                    $output = array(
                            'type' => $type,
                            'msg' => $msg,
                            'data'=>$data,
                            'errors'=>$errors
                    );

                    echo json_encode($output);
                    exit();
                }
                $this->initModules();
                $this->scripts[]='/public/js/jquery.form.js';
		$this->scripts[]='/public/js/edit.js';
            
		return $this->_render('admin/abstract/edit.tpl');
	}
        
        public function deleteAction(){
            $id = (int)$this->params()->fromQuery('id');
            if (!$id)  return $this->redirect()->toUrl('/admin/pages');
            $request = $this->getRequest();
            $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
			$repo = $objectManager->getRepository('Admin\Entity\\'.$this->entity);
			$res = $repo->find($id);
			
			if ($res) {
				$objectManager->remove($res);
				$objectManager->flush();
			}
            
            return $this->redirect()->toUrl('/admin/pages');
        }


		protected function addSeo()
		{
		
			$tab = count($this->config['tabs']);	
			$seo = array(
				'metaTitle'=>array(
	                    'tab'=>$tab,
						'name'=>'metaTitle',
						'label'=>'META title',
						'placeholder'=>'',
						'required'=>false,
						'type'=>'text'	   
				),
				'metaDescription'=>array(
	                'tab'=>$tab,
					'name'=>'metaTescription',
					'label'=>'META description',
					'placeholder'=>'',
					'required'=>false,
					'type'=>'text'
				),
				'metaKeywords'=>array(
                    'tab'=>$tab,
					'name'=>'metaDescription',
					'label'=>'META keywords',
					'placeholder'=>'',
					'required'=>false,
					'type'=>'text'
				)
			);
			
			$this->config['tabs'][$tab] = 'SEO';
			$this->config['fields'] = array_merge($this->config['fields'],$seo);			
		}
}

?>

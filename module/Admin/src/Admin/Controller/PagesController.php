<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class PagesController extends BaseController
{
        public $entity = 'XcPages';
    
	public function __construct()
	{
		$this->config = array(
			'primary'=> 'id',
			'fields'=>
			array(
				'id'=>array(
					'list'=>true, #show on grid list
					'name'=>'id',
					'label'=>'ID',
					'placeholder'=>'ID',
					'type'=>'text',
					'required'=>false,
					'disabled'=>1,
				),
				
				'path'=>array(
					'list'=>true, #show on grid list
					'name'=>'path',
					'label'=>'Path',
					'placeholder'=>'Path',
					'type'=>'text',
					'required'=>false,
					'disabled'=>1,
				),
                               
                            /*
                                'images'=>array(
                                            		'name'=>'images',
							'label'=>'Images',
							'type'=>'images',
                                                        'options' => array(
                                                                'uploadDir' => '/uploads/pages/images',
                                                                'uploadSize' => 2000,
                                                                'multi' => true,
                                                                'thumbnails' => array(
                                                                    array(96,96),
                                                                    array(128,128),
                                                                    array(300,300),
                                                                    array(800,600)
                                                                )
                                                        )
						   
				),
                            */
                                
                                'parent'=>array(
                    		'name'=>'parent',
							'label'=>'Select page',
							'type'=>'select',
							'class'=>'cm-dropkick',
                            'options' => array(
                                    'empty_option' => 'Please select an author',
                                    'value_query' => array(
                                        'entity'=>'XcPages',
                                        'key'=>'id',
                                        'value'=>'title'
                                    ) 
                            )
						   
				),
                            
				'title'=>array(
					'list'=>true, #show on grid list
					'name'=>'title',
					'label'=>'Title',
					'placeholder'=>'type something ...',
					'type'=>'text',
					'class'=>'',
					'indent'=>5,
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 5,
								'max'      => 100,
							),
						),
					)
				),
				
				'content'=>array(
					'name'=>'content',
					'label'=>'Content',
					'placeholder'=>'type something ...',
					'type'=>'textarea',
					'class'=>'cm-editor',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 1,
								'max'      => 500,
							),
						),
					)
				),
			
				'position'=>array(
							'list'=>true, #show on grid list
							'name'=>'position',
							'label'=>'Position',
							'placeholder'=>'1',
							'type'=>'text'
						   
				),
				'views'=>array(
							'list'=>true, #show on grid list
							'name'=>'views',
							'label'=>'Views',
							'placeholder'=>'0',
							'required'=>false,
							'disabled'=>1,
							'type'=>'text',
							'filters'  => array(
							array('name' => 'Int')
							)
				),
			),
			'use'=>array(),
			'labels'=>array(
				'title'=>'Pages',
				'add'=>'Add new page',
				'edit'=>'Edit page',
				'delete'=>'Delete page'
			),
			
			'modules'=>array('editor','dropkick'),
			'tabs'=>array('Default')
		);
		
		$this->addSeo();
		
	}
        
        protected function prepareSelect($entity,$key,$value)
        {
            $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $repo = $objectManager->getRepository('Admin\Entity\\'.$entity);
           
            $res = $repo->createQueryBuilder('p')
            ->select('p.'.$key.' as id,p.'.$value.' as value')
            ->where('p.parent = 0')
            ->orderBy('p.parent', 'ASC')
            ->getQuery()
            ->getArrayResult();
            
            foreach($res as $k=>$r)
                $rr[$r['id']]=$r['value'];
            
            
            
            return $rr;
        }
	
	
	protected function before_list($d,$sorting)
	{
		$d->addOrderBy('x.path','ASC');
		
		if($sorting[0]!='position') $d->addOrderBy('x.position','ASC');
	}
	
	
	protected function after_edit($data)
	{
		$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $repo = $objectManager->getRepository('Admin\Entity\\'.$this->entity);
		$res = $repo->findAll();
		foreach($res as $r)
		{
			if($r->getParent()==0) { $path = $r->getId(); $depth=0; }
			else { $path = $path = $r->getParent().'_'; $depth=1; }
			
			$r->setDepth($depth);
			$r->setPath($path);
			
			$objectManager->persist($r);
			$objectManager->flush();  
		}
	}
	
	protected function prepareData($data)
	{
		$default = array(
		'content'=>'',
		'menu'=>'',
		'route'=>'',
		'subtitle'=>'',
		'metaTitle'=>'',
		'const'=>'',
		'depth'=>0,
		'parent'=>0,
		'path'=>'',
		'views'=>0,
		'target'=>'_self',
		'alt'=>'',
		'rel'=>'',
		'thumb'=>'',
		'externalLink'=>'',
		'isOff'=>'',
		'countGallery'=>'',
		'countFiles'=>'',
		'views'=>0
		);
	
	
		return array_merge($default,$data);
	}
}
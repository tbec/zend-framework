<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class AccountsController extends BaseController
{
        public $entity = 'XcAccounts';
        
	public function __construct()
	{
		$this->config = array(
			'primary'=> 'idAccount',
			'fields'=>
			array(
				'idAccount'=>array(
					'list'=>true, #show on grid list
                    'name'=>'idAccount',
					'label'=>'ID',
                	'placeholder'=>'ID',
					'type'=>'text',
					'required'=>false,
					'disabled'=>1,
				),
				'login'=>array(
					'list'=>true, #show on grid list
                    'name'=>'login',
					'label'=>'Login',
                    'placeholder'=>'type something ...',
					'type'=>'text',
					'class'=>'',
					'required'=>true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 5,
								'max'      => 20,
							),
						),
					)
				),
                                'email'=>array(
								'list'=>true, #show on grid list
                                        'name'=>'email',
					'label'=>'Email',
                                        'placeholder'=>'type something ...',
					'type'=>'text',
					'class'=>'',
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 5,
								'max'      => 20,
							),
						),
					)
				),
                                'password'=>array(
                                        'name'=>'password',
					'label'=>'Password',
                                        'placeholder'=>'type something ...',
					'type'=>'password',
					'class'=>'',
					'required'=>false,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim')
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 5
							),
						),
					)
				),
			),
			'labels'=>array(
				'title'=>'Users accounts',
				'add'=>'Add new account',
				'edit'=>'Edit account',
				'delete'=>'Delete account'
			)
		);
		
	}

	protected function prepareData($data)
	{
		$default = array(
		'login'=>'',
		'email'=>'',
		'password'=>''
		);
		
		return array_merge($default,$data);
	}
}
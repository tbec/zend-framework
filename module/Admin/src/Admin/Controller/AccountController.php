<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Form\Annotation\AnnotationBuilder;
use Admin\Forms\LoginForm;
class AccountController extends ActionController
{	
	public function getSessionStorage()
    {
        if (! $this->storage) {
             $this->getServiceLocator()->get('doctrine.authenticationservice.orm_default');
        }
         
        return $this->storage;
    }

    public function indexAction()
    {  
	   $auth = $this->getAuth();
	   if (!$auth->hasIdentity()) return $this->redirect()->toUrl('/admin/account/login');
	   return $this->_render();
	}
	
	
	public function authenticate($login,$password)
	{
		$auth = $this->getAuth();
		
	   
        
        $auth->getAdapter()->setIdentityValue($login);
        $auth->getAdapter()->setCredentialValue(sha1($password));
        return $auth->authenticate();  
	}
	
	
	public function loginAction()
	{  
		
		$request = $this->getRequest();
		$form = new LoginForm();
 
		if($request->isPost()) {			
			$login = $request->getPost('login');
			$password = $request->getPost('password');
			$data = $errors = array();
			
			$objectManager = $this
			->getServiceLocator()
			->get('Doctrine\ORM\EntityManager');
			$objectManager->getRepository('Admin\Entity\XcAccounts');
			$account = new \Admin\Entity\XcAccounts();
			
            $form->setInputFilter($account->getInputFilter());
            $form->setData($request->getPost());
			
            if ($form->isValid()) {
                
			$result = $this->authenticate($login,$password);
			$code = (int)$result->getCode();
		
			if($code>0) 
			{
				$auth = $this->getAuth();
				if ($auth->getIdentity()->getType()<2)  
				{
					$auth->clearIdentity();
					list($type,$title,$msg,$data,$errors) = array(0,'Error','Access denied',$data,$errors);
				}
				
				else list($type,$title,$msg,$data,$errors) = array(1,'Success','You have been successfully logged in',array('redirect'=>'/admin'),$errors);
			}
			else list($type,$title,$msg,$data,$errors) = array(0,'Error','Incorrect login',$data,$errors);
			
			$result = array('type'=>$type,'title'=>$title,'msg'=>$msg);
			
			
			}
			else
			{
				foreach($form->getMessages() as $k=>$m)
				{
				$errors[$k] = array_shift($m);
				}
			
				list($type,$title,$msg,$data,$errors) = array(0,'Error','Incorrect login','',$errors);	
			}
			
			$result = array('type'=>$type,'title'=>$title,'msg'=>$msg,'data'=>$data,'errors'=>$errors,'redirect'=>'/admin');
			if($msg) $this->view_data['msg'] = $result;
			
			if($request->isXmlHttpRequest()) return $this->_json($result);
			else $this->view_data['msg'] = $result;
		}
		
		$this->layout('layout/custom');
		$this->view_data['form'] = $form;

		
		$auth = $this->getAuth();
		if ($auth->hasIdentity()) return $this->redirect()->toUrl('/admin/account');
		
		return $this->_render();
	}
	
	public function logoutAction(){
		$auth = $this->getAuth();
		if ($auth->hasIdentity()) $auth->clearIdentity();
		return $this->redirect()->toUrl('/admin/account/login');
	}
}
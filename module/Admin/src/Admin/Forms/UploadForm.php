<?php
namespace Admin\Forms;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\FileInput;

class UploadForm extends Form
{
    protected $inputFilter;
    
    public function __construct($name = null)
    {
        parent::__construct('upload-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'upload-form');
        $this->setAttribute('class', 'upload-form');
        $this->setAttribute('action', '/admin/upload/upload');
	$this->setAttribute('enctype','multipart/form-data');	
	
        
        $file = new Element\File('file');
        $file->setLabel('File')
        ->setAttribute('id', 'file')
        ->setAttribute('multiple', false);
        $this->add($file);
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Upload',
                'id' => 'submitbutton',
                'class'=>'button big'
            ),
        ));
    }
    
    public function getInputFilter() {
       
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
              
            
            $inputFilter->add(
                $factory->createInput(array(
                    'name'     => 'file',
                    'required' => true,
                ))
            );
             
            $this->inputFilter = $inputFilter;
        }
         
        return $this->inputFilter;
    }
}
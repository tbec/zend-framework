{$this->form()->openTag($form)}
        <fieldset>
            {foreach from=$form item=f}
			<div class="row {if $this->formElementErrors($f)}error{/if}">
					{$this->formElement($f)}
					{if $this->formElementErrors($f)}
						<div class="validity-tooltip" id="e_{$f->getAttribute('id')}" style="right:0px; bottom:0px">{$this->formElementErrors($f)}<div class="validity-tooltip-outer"><div class="validity-tooltip-inner"></div></div></div>
					{/if}
			</div>
			{/foreach}
        </fieldset>
{$this->form()->closeTag()}
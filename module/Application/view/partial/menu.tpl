<ul> 
	{foreach from=$this->container item=page} 
		<li {if $page->isActive()}'class="active"{/if}> 
			<a href="{$page->getHref()}">{$page->getLabel()}</a> 
			{if !empty($page->pages)} 
				<ul class="submenu"> 
					{foreach from=$page->pages item=subpage}
					<li {if $subpage->isActive()}'class="active"{/if}> 
						<a href="{$subpage->getHref()}">{$subpage->getLabel()}</a> </li> 
					{/foreach}
				</ul> 
			{/if}
		</li> 
	{/foreach}
</ul>

<div style="position:relative">
    <div class="ovinfo">
        Drop files here or
        <input id="file_upload" name="file_upload" type="file" class="file_upload" multiple="true">
    </div>
    <div class="queue"></div>
</div>

<div id="queue"></div>
<!--a style="position: relative; top: 8px;" href="javascript:$('#file_upload').uploadifive('upload')">Upload Files</a-->


{literal}
    
<style type="text/css">
    .queue { width:678px; margin:10px 0px;  border: 2px dashed #DDD; min-height: 200px }
    .ovinfo { background-color:#fff; position:absolute; z-index:3; color:#ccc; font-size:20px; left:50%; width:200px; height:70px; margin-top:-35px; top:50%; text-align:center; margin-left:-100px; }
    .uploadifive-button { margin-left:50px; margin-top:15px; }
</style>
<script type="text/javascript">
        $(function() {
                $('#file_upload').uploadifive({
                        'auto'             : false,
                        'checkScript'      : 'check-exists.php',
                        'formData'         : { },
                        'queueID'          : 'queue',
                        'uploadScript'     : 'uploadifive.php',
                        'onUploadComplete' : function(file, data) { console.log(data); }
                });
        });
</script>
{/literal}
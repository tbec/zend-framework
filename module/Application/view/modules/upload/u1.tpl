<script type="text/javascript">
(function() {
    
var bar = $('.bar');
var percent = $('.percent');
var status = $('#status');
   
$('#f1').ajaxForm({
    beforeSend: function() {
        status.empty();
        var percentVal = '0%';
        bar.width(percentVal)
        percent.html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        bar.width(percentVal)
        percent.html(percentVal);
    },
    success: function() {
        var percentVal = '100%';
        bar.width(percentVal)
        percent.html(percentVal);
		alert('ok')
    },
	complete: function(xhr) {
		status.html(xhr.responseText);
	}
}); 

})();       
</script>
<style>
.progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
.percent { position:absolute; display:inline-block; top:3px; left:48%; }
</style>


<form id="f1" action="/index" method="post" enctype="multipart/form-data">
	<input type="file" name="myfile"><br>
	<input type="submit" value="Upload File to Server">
</form>

<div class="progress">
	<div class="bar"></div >
	<div class="percent">0%</div >
</div>

<div id="status"></div>
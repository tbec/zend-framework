{capture name="scripts"}
{$this->headScript()
->appendFile("/js/uploader/jquery.uploadifive.js", "text/javascript")}
{/capture}

<div class="queue_cont"style="position:relative">
    <div class="ovinfo{$id}" {if $value}style="background-image:url({$value})"{/if}>
        Drop thumbnail here or
        <input id="file_upload{$id}" name="{$name}" type="file" value="{$value}" class="file_upload" multiple="true">
    </div>
    <div class="queue{$id}"></div>
</div>

<div id="queue{$id}"></div>
<!--a style="position: relative; top: 8px;" href="javascript:$('#file_upload').uploadifive('upload')">Upload Files</a-->


{literal}
    
<style type="text/css">
    .queue_cont:hover .ovinfo{/literal}{$id}{literal}{ opacity:1; }
    #queue{/literal}{$id}{literal} { width:{/literal}{$width}{literal}px }
    .queue{/literal}{$id}{literal} { width:{/literal}{$width}{literal}px; margin:10px 0px; border: 1px dashed #ddd; height: {/literal}{$height}{literal}px }
    .ovinfo{/literal}{$id}{literal} { opacity:.3; background-color:#fff; position:absolute; z-index:3; color:#333; font-size:15px; left:{/literal}{$width/2}{literal}px; width:100px; height:70px; margin-top:-50px; top:50%; text-align:center; margin-left:-50px; }
    .uploadifive-button { margin-top:15px; }
</style>
<script type="text/javascript">
        $(function() {
                $('#file_upload{/literal}{$id}{literal}').uploadifive({
                        'auto'             : {/literal}{$auto}{literal},
                        'checkScript'      : 'check-exists.php',
                        'formData'         : { },
                        'queueID'          : 'queue{/literal}{$id}{literal}',
                        'uploadScript'     : 'uploadifive.php',
                        'onUploadComplete' : function(file, data) { console.log(data); }
                });
        });
</script>
{/literal}
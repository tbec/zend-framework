{literal}
<style type="text/css">
    .s1_social_icons { width:100%; float:left; }
    .s1_social_icons li { float:left; list-style:none;}
	.s1_social_icons li a {  display:block; margin-left:5px; width:48px; height:48px; background-image:url(/application/view/modules/blocks/social_icons/img/s1.png)  }
	.s1_social_icons li a.tw{ background-position:0px -48px }
	.s1_social_icons li a.rs{ background-position:0px -96px }
	.s1_social_icons li a.mg{ background-position:0px -144px }
</style>
{/literal}

<ul class="s1_social_icons {if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
    <li><a href="" class="fb" /></a>
	<li><a href="" class="tw" /></a>
	<li><a href="" class="rs" /></a>
	<li><a href="" class="mg" /></a>
</ul>
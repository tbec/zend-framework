{literal}
<style type="text/css">

</style>
{/literal}

<div class="image_wrap">
	{if $image['link']}<a {if $image['swap']}class="swap" swap-src="{$image['swap-src']}"{/if} href="{$image['link']}">{/if}
	<img src="{$image['src']}" alt="{$image['alt']}"/>
	{if $link}</a>{/if}
</div>
{literal}
<style type="text/css">
.pricing_container {
	width: 780px;
	margin: 100px auto;
}
.pricing_container .top {
  width: 220px; height: 30px;
  background: #2F2E35;
  
  -webkit-border-radius: 10px 10px 0 0;
  -moz-border-radius: 10px 10px 0 0;
  border-radius: 10px 10px 0 0;
}
.pricing_container .top h2 {
  color: #fff;
  text-align: center;
  font: 300 15px/25px Helvetica, Verdana, sans-serif;
  line-height:30px;
}
.pricingtable {
  width: 220px; height: 360px;
  background: white;
  margin: 5px;
  float: left;
  
  -webkit-border-radius: 10px;
     -moz-border-radius: 10px;
          border-radius: 10px;
  -webkit-box-shadow: 2px 2px 9px rgba(0,0,0,0.3);
     -moz-box-shadow: 2px 2px 9px rgba(0,0,0,0.3);
          box-shadow: 2px 2px 9px rgba(0,0,0,0.3);
}
.pricingtable ul {
  list-style-type: none;
  font: 300 15px/2 Helvetica, Verdana, sans-serif;
  margin: 10px 0 0 15px;
}
.pricingtable ul strong {
  font-weight: bold;
}
.pricingtable hr {
  border: 0;
  background-color: #BCBEC0;
  color: #BCBEC0;
  height: 1px;
  width: 190px;
  margin: 20px 0 0 30px;
}
.pricingtable h1 {
  text-align: center;
  font: bold 88px/1 Helvetica, Verdana, sans-serif;
  margin: 20px 0 0 0;
}
.pricingtable h1 sup {
  font-size: 45px;
}
.pricingtable a { line-height:15px; margin-top:15px }
.pricingtable p {
  text-align: center;
  font: 500 14px/1 Helvetica, Verdana, sans-serif;
  color: #BCBEC0;
}

.featured {
	-webkit-transform: scale(1.2, 1.2);
	-moz-transform: scale(1.2, 1.2);
	-o-transform: scale(1.2, 1.2);
	-ms-transform: scale(1.2, 1.2);
	transform: scale(1.2, 1.2);
}
</style>
{/literal}

<div class="pricing_container">
    <div class="pricingtable">
      <div class="top">
        <h2>Basic</h2>
      </div>
      <ul>
        <li><strong>Full</strong> Email Support</li>
        <li><strong>25GB</strong> of Storage</li>
        <li><strong>5</strong> Domains</li>
        <li><strong>10</strong> Email Addresses</li>
      </ul>
    
      <hr />
      
      <h1><sup>$</sup>25</h1>
      <p>per month</p>
      
      <a class="btn" href="#">Sign Up</a>
</div>
    
<div class="pricingtable featured">
      <div class="top">
        <h2>Pro</h2>
      </div>
      <ul>
        <li><strong>Full</strong> Email Support</li>
        <li><strong>50GB</strong> of Storage</li>
        <li><strong>10</strong> Domains</li>
        <li><strong>20</strong> Email Addresses</li>
      </ul>
    
      <hr />
      
      <h1><sup>$</sup>49</h1>
      <p>per month</p>
      
      <a class="btn" href="#">Sign Up</a>
    </div>
    
    <div class="pricingtable">
      <div class="top">
        <h2>Premium</h2>
      </div>
      <ul>
        <li><strong>Full</strong> Email Support</li>
        <li><strong>Unlimited</strong> Storage</li>
        <li><strong>Unlimited</strong> Domains</li>
        <li><strong>50</strong> Email Addresses</li>
      </ul>
    
      <hr />
      
      <h1><sup>$</sup>79</h1>
      <p>per month</p>
      
      <a class="btn" href="#">Sign Up</a>
    </div>

</div>
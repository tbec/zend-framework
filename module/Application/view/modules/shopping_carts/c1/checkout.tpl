<div class="xc_header">Checkout<div class="xc_close" onclick="$(this).parent().parent().hide()"></div></div>

<div class="xc_content">
{include file="default/xcart/cart.tpl"}
</div>

<div class="xc_details">
	<div class="xc_shipping">
		<div class="xc_title">Shipping method</div>
		
		<ul>
			{if $total>100}
			<li><label><input type="radio" value="1" checked="checked" name="shipping" />Custom shipping - 20,00 PLN</label></li>
			{else}
			<li><label><input type="radio" value="2" checked="checked" name="shipping" />Custom shipping - 50,00 PLN</label></li>
			{/if}
		</ul>
	</div>

	<div class="xc_payment">
		<div class="xc_title">Payment method</div>
		<ul>
			<li><label><input type="radio" value="1" checked="checked" name="payment" />VISA</label></li>
			<li><label><input type="radio" value="1" name="payment" />PayPal</label></li>
			<li><label><input type="radio" value="1" name="payment" />platnosci.pl</label></li>
			<li><label><input type="radio" value="1" name="payment" />transferuj.pl</label></li>
		</ul>
	</div>
</div>



<div class="xc_buttons">
	<input type="submit" value="Buy" />
</div>
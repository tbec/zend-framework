{assign var="total" value=0}
{foreach from=$cart item=c}
{assign var="total" value=$total+$c.price_brutto*$c.quantity}
<div class="xc_item">
	<div class="xc_thumb"></div>
	<div class="xc_title">{$c['name']} ({$c['price_brutto']}) - {$c['quantity']}<div class="xc_remove" onclick="return cart.update({$c.id_cart},0)"></div></div>
</div>
{/foreach}

<div class="total">
	Total: {$total}
</div>
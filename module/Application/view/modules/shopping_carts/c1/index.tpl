<section class="categories grid_6 top_2">            
		<div class="title"><div></div>KATEGORIE</div>
		{include width=6 id="5" file="modules/blocks/categories/c1.tpl"}
</section>

<section class="push_1 grid_17">
	{foreach from=$products item=p}
		<div class="product xc p_{$p.id_product} {if $p.attributes}opt{/if}" style="width:200px; height:200px">
			<div class="xc_wrap">
				<div class="xc_desc">
				Lorem ipsum dolor
				</div>
				<div class="xc_info">
					<div class="avail">Availability: <span>{$p.availability}<span></div>
					<div class="feature">Sizes: <span>XS, S, L, XL, XXL<span></div>
				</div>
				
				{include file="modules/blocks/images/i1.tpl" image=$p['image']}
			</div>
		</div>
	{/foreach}
</section>


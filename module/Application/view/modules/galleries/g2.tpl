<link rel="stylesheet" type="text/css" href="{$base}/public/js/jqzoom/jqzoom.css" media="screen" />
<script type="text/javascript" src="{$base}/public/js/jqzoom/jqzoom.js"></script>
<script type="text/javascript">$(document).ready(function(){ $('a#jqzoom').jqzoom() })</script>
<div class="product_gallery {if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}" style="width:360px; text-align:left">
  <div class="bigImage_w">
  	<a href="{$base}{$k['galeria'][0]['folder']}800x600_{$k['galeria'][0]['filename']}" rel="gal1" class="fancybox bigImage" id="jqzoom">
  	<img   style="width:{$ug[3]}px;height:{$ug[4]}px" src="{$base}{$k['galeria'][0]['folder']}208x156_{$k['galeria'][0]['filename']}" alt="{$k['galeria'][0]['alt']}"/>
  	</a>
  </div>
  
  <table id="thumblist" style="width:100%; margin-top:20px">
  <tr>
  {$i=1}
  {foreach from=$k['galeria'] item=s key=j}
  <td class="image">
  <a  rel="{ gallery: 'gal1', smallimage: '{$base}{$s['folder']}128x96_{$s['filename']}',largeimage: '{$base}{$s['folder']}800x600_{$s['filename']}'}" href="javascript:void(0);" class="fancybox" rel="fancybox"  href="{$base}{$s['folder']}800x600_{$s['filename']}"><img src="{$base}{$s['folder']}128x96_{$s['filename']}"></a>
  </td>
  {if $i%$ug[0]==0}</tr><tr>{/if}
  {$i=$i+1}
  {/foreach}
  </tr>
  </table>
  </div>
  
  
{capture name="scripts"}
{$this->headLink()
	->appendStylesheet("/js/dropkick/dropkick.css")}
	
{$this->headScript()
->appendFile("/js/dropkick/dropkick.js", "text/javascript")}
{/capture}
<input {if $disabled}disabled="disabled"{/if}  name="{$name}" type="checkbox" class="switch" id="{$id}" value="1" /><label class="switch_label" for="{$id}">{$label}</label>

{literal}
<script type="text/javascript">
    jQuery(function($){
     // Replace checkboxes with switch
        $("input[type=checkbox].switch").each(function() {

                // Insert switch
                $(this).before('<span class="switch"><span class="background" /><span class="mask" /></span>');

                // Hide checkbox
                $(this).hide();

                // Set inital state
                if (!$(this)[0].checked) $(this).prev().find(".background").css({left: "-56px"});
        });

        $("span.switch").click(function() {
                
                // Slide switch off
                if ($(this).next()[0].checked) {
                        $(this).find(".background").animate({left: "-56px"}, 200);
                        
                        
                // Slide switch on
                } else {
                        $(this).find(".background").animate({left: "0px"}, 200);
                        
                }

                // Toggle state of checkbox
                $(this).next()[0].checked = !$(this).next()[0].checked;
        });
    })
</script>
{/literal}
{capture name="scripts"}
{$this->headLink()
	->appendStylesheet("/js/tagit/tagit.css")}
{$this->headScript()
->appendFile("/js/tagit/tagit.js", "text/javascript")}
{/capture}

<input type="text" name="{$name}" id="{$id}" value="{$value}" placeholder="{$label}" />

<script type="text/javascript">
    jQuery(function($){
        $('#{$id}').tagit({ 
            allowDuplicates:false,
            tagLimit:20,
            caseSensitive:false,
            allowSpaces:true,
            placeholderText: '{$label}'
        });
    })
</script>
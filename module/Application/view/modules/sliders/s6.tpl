<link rel="stylesheet" type="text/css" href="{$base}/modules/sliders/s6/s6.css" media="screen" />
<script type="text/javascript" src="{$base}/modules/sliders/s6/s6.js"></script>




<div id="{$id}" style="position:relative" class="{if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
	<div class="s6_cont ">
		<div class="s6_wrapper" {if $height}style="height:{$height}px"{/if}>
		
			<div class="carousel">
				<img src="http://coolcarousels.frebsite.nl/c/69/img/t2.jpg" alt="Call Of Duty" class="grid_{$grid}"/>
				<img src="http://coolcarousels.frebsite.nl/c/69/img/t3.jpg" alt="Call Of Duty1" class="grid_{$grid}"/>
				<img src="http://coolcarousels.frebsite.nl/c/69/img/t4.jpg" alt="Call Of Duty2" class="grid_{$grid}"/>
				<img src="http://coolcarousels.frebsite.nl/c/69/img/t5.jpg" alt="Call Of Duty3" class="grid_{$grid}"/>
			</div>
		</div>
		<div class="pager"></div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
				$('.s6_wrapper .carousel').carouFredSel({
					direction: 'up',
					items: 1,
					scroll: {
						fx: 'directscroll'
					},
					pagination: {
						container: '.s6_cont .pager',
						anchorBuilder: function( nr ) {
							return '<a href="#" class="thumb' + nr + '"><img src="' + this.src + '" width="50" /></a>';
						}
					}
				});
			});
</script>
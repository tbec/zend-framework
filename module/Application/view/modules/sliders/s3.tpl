<link rel="stylesheet" type="text/css" href="{$base}/modules/sliders/s3/s3.css" media="screen" />
<script type="text/javascript" src="{$base}/modules/sliders/s3/s3.js"></script>

<div id="{$id}" style="position:relative" class="{if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
		<div class="s3_wrapper">
			<div id="{$id}_gallery" class="s3_carousel">
				<div><img src="http://coolcarousels.frebsite.nl/c/69/img/t2.jpg"/></div>
				<div><img src="http://coolcarousels.frebsite.nl/c/69/img/t3.jpg"/></div>
				<div><img src="http://coolcarousels.frebsite.nl/c/69/img/t4.jpg"/></div>
				<div><img src="http://coolcarousels.frebsite.nl/c/69/img/t5.jpg"/></div>
				<div><img src="http://coolcarousels.frebsite.nl/c/69/img/t6.jpg"/></div>
			</div>
			<div class="s3_title">
				<h3>The Netherlands covered in snow</h3>
				<p><a href="http://gigapica.geenstijl.nl/2013/01/the_netherlands_covered_in_sno.html">images from gigapica.geenstijl.nl</a></p>
			</div>
			<div id="s3_pager"></div>
		</div>
</div>

<script type="text/javascript">
$(function() {
	$('#{$id}_gallery').carouFredSel({
		responsive: true,
		items: {
			visible: 1,
			width: 900,
			height: 500
		},
		scroll: {
			duration: 250,
			timeoutDuration: 2500,
			fx: 'uncover-fade'
		},
		pagination: '#pager'
	});
});
</script>
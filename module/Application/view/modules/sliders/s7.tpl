<link rel="stylesheet" type="text/css" href="{$base}/modules/sliders/s7/s7.css" media="screen" />
<script type="text/javascript" src="{$base}/modules/sliders/s7/s7.js"></script>




<div id="{$id}" style="position:relative" class="{if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
	<div class="s7_cont ">
		<div class="s7_wrapper" {if $height}style="height:{$height}px"{/if}>
			
			<div class="carousel">
				<div>
					<h3>Geneva International Motor Show</h3>
					<p>The Geneva International Motor Show will open to the public from 03 to 13 March, presenting more than 260 exhibitors and more than 175 world and European premieres.</p>
				</div>
				<div class="images">
				{foreach from=$images['gallery'] item=i key=k}
				<img src="{$base}{$i['folder']}{$i['filename']}" alt="" class="grid_{$grid}" />
				{/foreach}
				</div>
			</div>
			<div class="pager"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('.s7_wrapper').hover(
			function() {
				$('.s7_wrapper .pager').addClass( 'visible' );
				$('.s7_wrapper .carousel').trigger( 'next' );
			}, function() {
				$('.s7_wrapper .pager').removeClass( 'visible' );
				$('.s7_wrapper .carousel').trigger( 'prev' );
			}
		);
		$('.s7_wrapper .carousel').carouFredSel({
			circular: false,
			infinite: false,
			direction: 'up',
			auto: false,
			scroll: {
				queue: 'last'
			}
		});
		$('.s7_wrapper .images').carouFredSel({
			circular: false,
			infinite: false,
			auto: false,
			pagination: '.s7_wrapper .pager'
		});
	});
</script>
<link rel="stylesheet" type="text/css" href="{$base}/modules/sliders/s4/s4.css" media="screen" />
<script type="text/javascript" src="{$base}/modules/sliders/s4/s4.js"></script>




<div id="{$id}" style="position:relative" class="{if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
		<div class="s4_wrapper" {if $height}style="height:{$height}px"{/if}>
			<div class="s4_images">
				<div class="carousel">
					<img src="http://coolcarousels.frebsite.nl/c/69/img/t2.jpg" class="grid_{$grid}"/>
					<img src="http://coolcarousels.frebsite.nl/c/69/img/t3.jpg" class="grid_{$grid}"/>
					<img src="http://coolcarousels.frebsite.nl/c/69/img/t4.jpg" class="grid_{$grid}"/>
					<img src="http://coolcarousels.frebsite.nl/c/69/img/t5.jpg" class="grid_{$grid}"/>
				</div>
			</div>
			<div class="s4_timer grid_{$grid}"></div>
			<div class="s4_captions grid_{$grid}">
				<div class="carousel">
					<div class="cod grid_{$grid}">Call Of Duty
						<div class="pager"></div>
					</div>
					<div class="gta grid_{$grid}">Grand Theft Auto
						<div class="pager"></div>
					</div>
					<div class="mgs grid_{$grid}">Metal Gear Solid
						<div class="pager"></div>
					</div>
				</div>
			</div>
		</div>
</div>

<script type="text/javascript">
	$(function() {
		var $imgs = $('.s4_images .carousel'),
			$capt = $('.s4_captions .carousel'),
			$timr = $('.s4_timer');

		$imgs.carouFredSel({
			circular: false,
			scroll: {
				easing: 'quadratic',
				duration: 2,
				timeoutDuration: 3000,
				onBefore: function( data ) {
					$capt.trigger( 'slideTo', [ '.' + data.items.visible.first().attr( 'alt' ) ] );
					$timr.stop().animate({
						opacity: 0
					}, data.scroll.duration);
				},
				onAfter: function() {
					$timr.stop().animate({
						opacity: 1
					}, 150);
				}
			},
			auto: {
				progress: '.s4_timer'
			},
			pagination: {
				container: 's4_wrapper .pager',
				anchorBuilder: function( i ) {
					return '<a class="p' + i + '" href="#"></a>';
				}
			}
		});
		$capt.carouFredSel({
			circular: false,
			auto: false,
			scroll: {
				easing: 'quadratic',
				duration: 2
			}
		});
	});
</script>
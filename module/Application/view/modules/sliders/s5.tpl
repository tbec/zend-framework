<link rel="stylesheet" type="text/css" href="{$base}/sliders/s5/s5.css" media="screen" />
<script type="text/javascript" src="{$base}/modules/sliders/s5/s5.js"></script>




<div id="{$id}" style="position:relative" class="{if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
		<div class="s5_wrapper" {if $height}style="height:{$height}px"{/if}>
			<div class="carousel">
				<img src="http://coolcarousels.frebsite.nl/c/69/img/t2.jpg" alt="Call Of Duty" class="grid_{$grid}"/>
				<img src="http://coolcarousels.frebsite.nl/c/69/img/t3.jpg" alt="Call Of Duty1" class="grid_{$grid}"/>
				<img src="http://coolcarousels.frebsite.nl/c/69/img/t4.jpg" alt="Call Of Duty2" class="grid_{$grid}"/>
				<img src="http://coolcarousels.frebsite.nl/c/69/img/t5.jpg" alt="Call Of Duty3" class="grid_{$grid}"/>
			</div>
			
			<div class="pager"></div>
		</div>
</div>

<script type="text/javascript">
	$(function() {
		$('.s5_wrapper .carousel').carouFredSel({
			direction: 'up',
			auto: false,
			scroll: {
				duration: 1000
			},
			pagination: {
				container: '.s5_wrapper .pager',
				anchorBuilder: function() {
					return '<a href="#">'+ this.alt +'</a>';
				}
			}
		});
	});
</script>
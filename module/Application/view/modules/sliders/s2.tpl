{literal}
<style type="text/css">
.s2_slider{ position:relative; float:left; width:100%; height:150px; background:#eee }
.s2_slider label{ position:absolute; width:250px; bottom:10px; left:10px }
.s2_slider label .button { float:left }
.s2_slider label .button:last-child{ margin-left:10px }
.s2_slider .t { margin-bottom:20px; clear:both; text-align:left; width:100%; display:block; font-size:20px; float:left }
</style>
{/literal}

<div class="s2_slider {if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
    <label>
        <span class="t">Lorem ipsum dolor sit<br/>Do something</span>
        <a href="" class="button big">Make order</a>
        <a href="" class="button big">See demo</a>
    </label>
</div>
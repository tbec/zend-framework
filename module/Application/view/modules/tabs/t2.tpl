{assign var="p" value="`$id`_"}

<style type="text/css">
 .{$p}tabs a{ background-color:#ccc; float:left; width:100%; margin-right:10px; padding:5px 0px; text-indent:10px }  
 .{$p}tabs .{$p}tabs_content { float:left; width:100%; background-color:#eee; padding:0px 0px; text-align:left }
 .{$p}tabs .{$p}tabs_content div { height:200px; float:left; background:#eee; padding:0px 10px; display:none }
 .{$p}tabs .{$p}tabs_content div.active{  }
 .{$p}tabs .{$p}tabs_content div.show{ display:block }
</style>

<script type="text/javascript">
      jQuery(function($)
      {
        $('#{$id} a').click(function(){
            if($(this).hasClass('active')) return false;
            
            
            $('#{$id} .{$p}tabs_content  div,#{$id} .{$p}tabs_content a').removeClass('active');
            $('#{$id} .{$p}tabs_content .'+$(this).attr('id')+','+'#{$id} .{$p}tabs_content .'+$(this).attr('id')+' a').addClass('active');
            $('#{$id} .{$p}tabs_content  div').slideUp();
            $('#{$id} .{$p}tabs_content .'+$(this).attr('id')).slideDown();
            $(this).addClass('active');
            return false;
        })  
      })
</script>



<div id="{$id}" class="{$p}tabs {if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
    <div class="{$p}tabs_content">
    {section name="c" start=1 loop=$tabs+1 step=1}
    {assign var="i" value="lbl_`$smarty.section.c.index`"}   
    <a href="" id="{$p}tab{$smarty.section.c.index}" class="{if $smarty.section.c.index==$active}active{/if}">
            {if $i}
                {$i}
            {else}
                Label {$i} is not captured
            {/if}
    </a>
    
    
    {assign var="i" value="cnt_`$smarty.section.c.index`"}   
    <div class="{$p}tab{$smarty.section.c.index} {if $smarty.section.c.index==$active}active show{/if}">
            {if $smarty.capture.$i}
                {$smarty.capture.$i}
            {else}
                Tab {$i} is not captured
            {/if}
    </div>
    {/section}
    </div>
    
</div>
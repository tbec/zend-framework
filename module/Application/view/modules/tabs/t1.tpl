{assign var="p" value="`$id`_"}

<style type="text/css">
 .{$p}tabs a{ float:left; margin-right:10px; padding:5px 10px; background:#eee }  
 .{$p}tabs .{$p}tabs_content { float:left; width:100%; background-color:#eee; padding:10px 0px; text-align:left }
 .{$p}tabs .{$p}tabs_content div { padding:0px 10px; display:none }
 .{$p}tabs .{$p}tabs_content div.active { display:block }
</style>

<script type="text/javascript">
      jQuery(function($)
      {
        $('#{$id} a').click(function(){
            $('#{$id} .{$p}tabs_content  div').removeClass('active');
            $('#{$id} .{$p}tabs_content .'+$(this).attr('id')).addClass('active');
            return false;
        })  
      })
</script>



<div id="{$id}" class="{$p}tabs {if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
    
    {section name="c" start=1 loop=$tabs+1 step=1}
    {assign var="i" value="lbl_`$smarty.section.c.index`"}   
    <a href="" id="{$p}tab{$smarty.section.c.index}" class="{if $smarty.section.c.index==$active}active{/if}">
            {if $i}
                {$i}
            {else}
                Label {$i} is not captured
            {/if}
    </a>
    {/section}
    <div class="{$p}tabs_content">
    {section name="c" start=1 loop=$tabs+1 step=1}
    {assign var="i" value="cnt_`$smarty.section.c.index`"}   
    <div class="{$p}tab{$smarty.section.c.index} {if $smarty.section.c.index==$active}active{/if}">
            {if $smarty.capture.$i}
                {$smarty.capture.$i}
            {else}
                Tab {$i} is not captured
            {/if}
    </div>
    {/section}
    </div>
    
</div>


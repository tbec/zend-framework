{literal}
<style type="text/css">
    .c1_columns { width:100%; float:left; }
    .c1_columns .col { float:left;  min-height:200px }
    .c1_columns .col:first-child > div { margin-left:0px }
    .c1_columns .col > div { background-color:#eee; margin:5px; margin-right:0px; }
</style>
{/literal}

<div class="c1_columns {if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
    {section name="c" start=1 loop=$columns+1 step=1}
    <div class="col grid_{$grid/$columns}">
        <div>
            {assign var="i" value="col_`$smarty.section.c.index`"}
            {if $smarty.capture.$i}
                {$smarty.capture.$i}
            {else}
                Column {$i} is not captured
            {/if}
        </div>
    </div>
    {/section}
</div>
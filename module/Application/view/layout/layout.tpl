{$this->doctype()}

<html lang="en">
<head>
    <meta charset="utf-8">
    {$this->headTitle()->setSeparator(' - ')->setAutoEscape(false)}

    {$basePath = $this->basePath()}
    {$this->headLink()
	->appendStylesheet("`$basePath`/css/default.css")
	->appendStylesheet("`$basePath`/css/grid.css")
	->appendStylesheet("`$basePath`/js/menu/menu.css")
	->appendStylesheet("`$basePath`/css/skin.css")}

    {$this->headLink([ 'rel' => 'shortcut icon', 'type' => 'image/vnd.microsoft.icon', 'href' =>
    "`$basePath`/images/favicon.ico"])}


    {$this->headScript()
	->prependFile("`$basePath`/js/jquery.min.js", "text/javascript")
	->appendFile("`$basePath`/js/jquery_ui/jquery-ui-1.10.2.custom.js", "text/javascript")}
    {$this->headTitle('ZF2 Skeleton Application')}

    {*$this->headMeta()*}

    <!-- Styles -->
    {$this->headLink()}

    <!-- Scripts -->
    {$this->headScript()}	
	
    {$smarty.capture.scripts}

</head>

<body>

	<div>
	<div id="header" class="container_24">
		<header class="header">
				
				<div class="grid_6">
				<a href="/"><img style="margin-top:20px; float:left"src="{$basePath}/admin/public/images/logo.png" /></a>
				</div>
				
				<div id="cssmenu" class="grid_13">
					{$this->navigation('navigation')->menu()->setPartial('partial/menu')}
				</div>
				
				
				<dl class="grid-5 user_info">
				  <dt><a href="{$basePath}/account"><img src="{$basePath}/public/images/avatar.png" alt="" /></a></dt>
				  <dd> <a class="welcome_user" href="{$basePath}/account">Welcome, <strong>{if $user}{$user->getLogin()}{/if}</strong></a> <a class="logout" href="{$basePath}/account/logout">Logout</a></dd>
				</dl>
		</header>
	</div>
	</div>
	<div id="breadcrumbs" class="container_24 top_5">
	{$this->navigation('navigation')->breadcrumbs()->setLinkLast(true)->setMaxDepth(1)}
	</div>
	
	<div id="notifications" class="container_24 top_5 error">
	{if $msg}
		<div class="notify {if !$msg['type']}error{else}success{/if}box"> <h1>{$msg['title']}</h1> <span class="alerticon"></span> <p>{$msg['msg']}</p> </div>
	{/if}
	</div>
	
	
	<div id="content" class="container_24 top_5">

		{$this->content}
	</div>

	<div id="footer"  class="container_24 top_5">
		<footer style="background-color:#c16b2e; width:100%; padding:5px 10px; color:#fff">
		&copy; 2006 - 2012 by Zend Technologies Ltd. All rights reserved.
		</footer>
	</div>
        
        
</body>
</html>

{capture name="scripts"}
{$this->headScript()
->appendFile("/js/api/notification.js", "text/javascript")
->appendFile("/js/jquery.validity.js", "text/javascript")
->appendFile("/js/jquery.form.js", "text/javascript")
->appendFile("/js/api/user.js", "text/javascript")}
{/capture}
{* register-form *}
{include file="../forms/register.tpl" grid=9 push=3}

{* login-form *}
{include file="../forms/login.tpl" grid=6 push=5}
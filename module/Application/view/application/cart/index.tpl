<div class="grid_24">
	{if $total_items > 0}
	<h3>Products in cart ({$total_items}):</h3>
	<table style="width: 100%" border="1">
	<tr>
	  <th>Qty</th>
	  <th>Name</th>
	  <th>Item Price</th>
	  <th>Sub-Total</th>
	</tr>
	{foreach from=$items item=key}
	<tr>
		<td style="text-align: center;">{$key['qty']}</td>
		<td style="text-align: center;">
		{$key['name']}
			{if $key['options'] != 0}
				Options:
				{foreach from=$key['options'] key=options item=value}
					{$options} {$value}
				{/foreach}
			{/if}
		</td>
		<td style="text-align: center;">{$key['price']}</td>
		<td style="text-align: center;">{$key['sub_total']}</td>
	</tr>
	{/foreach}
	<tr>
	  <td colspan="2"></td>
	  <td style="text-align: center;"><strong>Sub Total</strong></td>
	  <td style="text-align: center;"> {$total['sub-total']}</td>
	</tr>
	<tr>
	  <td colspan="2"></td>
	  <td style="text-align: center;"><strong>Vat</strong></td>
	  <td style="text-align: center;"> {$total['vat']}</td>
	</tr>
	<tr>
	  <td colspan="2"></td>
	  <td style="text-align: center;"><strong>Total</strong></td>
	  <td style="text-align: center;"> {$total['total']}</td>
	</tr>
	</table>
	{else}
	<h4>The Shopping Cart Empty</h4>
	{/if}
</div>
<section id="user-profile" class="grid_24 fl" style="position:relative; top:-85px">
    <form>
        {include file="../../modules/upload/u3.tpl" width="1000" height="200" id="background" auto='false' name="background" value="{$userdata['background']}" design="image" label="Thumbnail" multiple=false extensions='*.jpg,*.jpeg,*.png,*.gif'}
    </form>
    
    <form id="user-profile-form" class="user-login-form" action="{$base}/account/login" method="post">
        <fieldset class="grid_7">
            {include file="../../modules/upload/u3.tpl" id="thumbnail" auto='false' name="thumbnail" value="{$userdata['thumbnail']}" width="200" height="200" design="image" label="Thumbnail" multiple=false extensions='*.jpg,*.jpeg,*.png,*.gif'}
        </fieldset>
        
        <fieldset class="grid_15">
			
				<div class="row grid_4">
					<input type="text" id="firstname" name="firstname" value="{$userdata['firstname']}" placeholder="Firstname" />
				</div>

				<div class="row grid_5 push_1">
					<input type="text" id="lastname" name="lastname" value="{$userdata['lastname']}" placeholder="Lastname" />
				</div>
            <div style="clear:both"></div>
            <div class="row">
                <input type="text" id="jobtitle" name="jobtitle" value="{$userdata['jobtitle']}" placeholder="Position" />
            </div>
            
            <div class="row">
                <input type="text" id="address" name="address" value="{$userdata['address']}" placeholder="Address" />
            </div>
            
            <div class="row grid_3">
                <input type="text" id="postal" name="postal" value="{$userdata['postal']}" placeholder="Postal" />
            </div>
            
            <div class="row grid_4" style="margin:0px 44px">
                <input type="text" id="city" name="city" value="{$userdata['city']}" placeholder="City" />
            </div>
            
            <div class="row">
                <select id="country" name="country" />
                    <option value="NL">Netherlands</option>
                </select>
            </div>
       
            <div class="grid_17 fr">
                <div class="row">
                    <textarea id="description" name="description" placeholder="Description" />{$userdata['description']}</textarea>
                </div>

                 <div class="row">
                    {include file="../../modules/form/switch.tpl" label="Availability" id="availability" name="availability" checked="{$userdata['availability']}"}
                </div>
                
                <div class="row">
                    {include file="../../modules/form/tags.tpl" label="add skill" id="skills" name="skills" value="{$userdata['skills']}"}
                </div>


                <div class="row">
                    <input type="submit" class="fr button big" value="Save" />
                </div>
            </div>
        </fieldset>
    </form>
</section>
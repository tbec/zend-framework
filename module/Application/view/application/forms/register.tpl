<section id="register" class="{if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
    <header>
        <h1 class="title"><div></div>Register</h1>
    </header>

    <form id="user-register-form" class="user-register-form" action="{$base}/account/register" method="post">
        <fieldset>
            <div class="row">
                <input type="text" id="login" name="login" value="" placeholder="{$this->translate('Login')}" maxsize="30" minsize="5" />
            </div>

            <div class="row">
                <input type="email" id="email" name="email" value="" placeholder="{$this->translate('Email')}" maxsize="50" />
            </div>

            <div class="row">
                <input type="password" id="password" name="password" value="" placeholder="{$this->translate('Password')}" maxsize="50" />
            </div>

            <div class="row">
                <input type="password" id="cpassword" name="cpassword" value="" placeholder="{$this->translate('Confirm password')}" maxsize="50" />
            </div>

            <div class="row">
                <input type="submit" class="button big" value="Create account" />
            </div>
        </fieldset>
    </form>
</section>
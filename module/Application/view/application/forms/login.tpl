

<section id="login" class="{if $push}push_{$push}{/if} {if $pull}pull_{$pull}{/if} {if $top}top_{$top}{/if} {if $bottom}bottom_{$bottom}{/if} grid_{$grid}">
    <header>
        <h1 class="title"><div></div>Log in</h1>
    </header>
	{include file='../../partial/form.tpl' form=$this->form}
</section>
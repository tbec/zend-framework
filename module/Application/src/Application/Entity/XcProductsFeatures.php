<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcProductsFeatures
 *
 * @ORM\Table(name="xc_products_features")
 * @ORM\Entity
 */
class XcProductsFeatures
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_produktu", type="bigint", nullable=false)
     */
    private $idProduktu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cechy", type="bigint", nullable=false)
     */
    private $idCechy;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProduktu
     *
     * @param integer $idProduktu
     * @return XcProductsFeatures
     */
    public function setIdProduktu($idProduktu)
    {
        $this->idProduktu = $idProduktu;
    
        return $this;
    }

    /**
     * Get idProduktu
     *
     * @return integer 
     */
    public function getIdProduktu()
    {
        return $this->idProduktu;
    }

    /**
     * Set idCechy
     *
     * @param integer $idCechy
     * @return XcProductsFeatures
     */
    public function setIdCechy($idCechy)
    {
        $this->idCechy = $idCechy;
    
        return $this;
    }

    /**
     * Get idCechy
     *
     * @return integer 
     */
    public function getIdCechy()
    {
        return $this->idCechy;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return XcProductsFeatures
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcProductsRelated
 *
 * @ORM\Table(name="xc_products_related")
 * @ORM\Entity
 */
class XcProductsRelated
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_produktu", type="bigint", nullable=false)
     */
    private $idProduktu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_produktu2", type="bigint", nullable=false)
     */
    private $idProduktu2;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProduktu
     *
     * @param integer $idProduktu
     * @return XcProductsRelated
     */
    public function setIdProduktu($idProduktu)
    {
        $this->idProduktu = $idProduktu;
    
        return $this;
    }

    /**
     * Get idProduktu
     *
     * @return integer 
     */
    public function getIdProduktu()
    {
        return $this->idProduktu;
    }

    /**
     * Set idProduktu2
     *
     * @param integer $idProduktu2
     * @return XcProductsRelated
     */
    public function setIdProduktu2($idProduktu2)
    {
        $this->idProduktu2 = $idProduktu2;
    
        return $this;
    }

    /**
     * Get idProduktu2
     *
     * @return integer 
     */
    public function getIdProduktu2()
    {
        return $this->idProduktu2;
    }
}
<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcStats
 *
 * @ORM\Table(name="xc_stats")
 * @ORM\Entity
 */
class XcStats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="resource_id", type="bigint", nullable=false)
     */
    private $resourceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="resource_type", type="integer", nullable=false)
     */
    private $resourceType;

    /**
     * @var integer
     *
     * @ORM\Column(name="ip", type="integer", nullable=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="browser", type="string", length=255, nullable=false)
     */
    private $browser;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=255, nullable=false)
     */
    private $lang;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", nullable=false)
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="referer", type="string", length=255, nullable=false)
     */
    private $referer;


}

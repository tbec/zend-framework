<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcProductsLang
 *
 * @ORM\Table(name="xc_products_lang")
 * @ORM\Entity
 */
class XcProductsLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="id_lang", type="boolean", nullable=false)
     */
    private $idLang;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="bigint", nullable=false)
     */
    private $idProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="vat", type="integer", nullable=false)
     */
    private $vat;

    /**
     * @var float
     *
     * @ORM\Column(name="price_brutto", type="decimal", nullable=false)
     */
    private $priceBrutto;

    /**
     * @var float
     *
     * @ORM\Column(name="price_netto", type="decimal", nullable=false)
     */
    private $priceNetto;

    /**
     * @var float
     *
     * @ORM\Column(name="price_promo", type="decimal", nullable=false)
     */
    private $pricePromo;

    /**
     * @var string
     *
     * @ORM\Column(name="cache", type="text", nullable=false)
     */
    private $cache;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=300, nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=300, nullable=false)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=300, nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=300, nullable=false)
     */
    private $tags;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLang
     *
     * @param boolean $idLang
     * @return XcProductsLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;
    
        return $this;
    }

    /**
     * Get idLang
     *
     * @return boolean 
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     * @return XcProductsLang
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;
    
        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer 
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return XcProductsLang
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return XcProductsLang
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return XcProductsLang
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set vat
     *
     * @param integer $vat
     * @return XcProductsLang
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    
        return $this;
    }

    /**
     * Get vat
     *
     * @return integer 
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set priceBrutto
     *
     * @param float $priceBrutto
     * @return XcProductsLang
     */
    public function setPriceBrutto($priceBrutto)
    {
        $this->priceBrutto = $priceBrutto;
    
        return $this;
    }

    /**
     * Get priceBrutto
     *
     * @return float 
     */
    public function getPriceBrutto()
    {
        return $this->priceBrutto;
    }

    /**
     * Set priceNetto
     *
     * @param float $priceNetto
     * @return XcProductsLang
     */
    public function setPriceNetto($priceNetto)
    {
        $this->priceNetto = $priceNetto;
    
        return $this;
    }

    /**
     * Get priceNetto
     *
     * @return float 
     */
    public function getPriceNetto()
    {
        return $this->priceNetto;
    }

    /**
     * Set pricePromo
     *
     * @param float $pricePromo
     * @return XcProductsLang
     */
    public function setPricePromo($pricePromo)
    {
        $this->pricePromo = $pricePromo;
    
        return $this;
    }

    /**
     * Get pricePromo
     *
     * @return float 
     */
    public function getPricePromo()
    {
        return $this->pricePromo;
    }

    /**
     * Set cache
     *
     * @param string $cache
     * @return XcProductsLang
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
    
        return $this;
    }

    /**
     * Get cache
     *
     * @return string 
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return XcProductsLang
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return XcProductsLang
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return XcProductsLang
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return XcProductsLang
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    
        return $this;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return XcProductsLang
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
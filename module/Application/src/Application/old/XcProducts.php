<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcProducts
 *
 * @ORM\Table(name="xc_products")
 * @ORM\Entity
 */
class XcProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="import_id", type="bigint", nullable=false)
     */
    private $importId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_manufacturer", type="bigint", nullable=false)
     */
    private $idManufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=500, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="availability", type="integer", nullable=false)
     */
    private $availability;

    /**
     * @var integer
     *
     * @ORM\Column(name="unit", type="integer", nullable=false)
     */
    private $unit;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=255, nullable=false)
     */
    private $symbol;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_category", type="bigint", nullable=false)
     */
    private $idCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_quantity", type="integer", nullable=false)
     */
    private $maxQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="promotion", type="integer", nullable=false)
     */
    private $promotion;

    /**
     * @var integer
     *
     * @ORM\Column(name="sold", type="integer", nullable=false)
     */
    private $sold;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity_hurt", type="integer", nullable=false)
     */
    private $quantityHurt;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255, nullable=false)
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="attributes", type="text", nullable=false)
     */
    private $attributes;


}

<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcCategoriesLang
 *
 * @ORM\Table(name="xc_categories_lang")
 * @ORM\Entity
 */
class XcCategoriesLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_category", type="bigint", nullable=false)
     */
    private $idCategory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="id_lang", type="boolean", nullable=false)
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255, nullable=false)
     */
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=300, nullable=false)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=300, nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=300, nullable=false)
     */
    private $metaTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;


}

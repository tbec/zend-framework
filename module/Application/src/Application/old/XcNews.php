<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcNews
 *
 * @ORM\Table(name="xc_news")
 * @ORM\Entity
 */
class XcNews
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=100, nullable=false)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=false)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="menu", type="integer", nullable=false)
     */
    private $menu;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="text", nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text", nullable=false)
     */
    private $metaKeywords;

    /**
     * @var integer
     *
     * @ORM\Column(name="const", type="integer", nullable=false)
     */
    private $const;

    /**
     * @var integer
     *
     * @ORM\Column(name="depth", type="bigint", nullable=false)
     */
    private $depth;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent", type="bigint", nullable=false)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=500, nullable=false)
     */
    private $path;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="bigint", nullable=false)
     */
    private $views;

    /**
     * @var string
     *
     * @ORM\Column(name="target", type="string", length=255, nullable=false)
     */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=255, nullable=false)
     */
    private $alt;

    /**
     * @var string
     *
     * @ORM\Column(name="rel", type="string", length=255, nullable=false)
     */
    private $rel;

    /**
     * @var string
     *
     * @ORM\Column(name="external_link", type="string", length=255, nullable=false)
     */
    private $externalLink;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_off", type="integer", nullable=false)
     */
    private $isOff;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_gallery", type="integer", nullable=false)
     */
    private $countGallery;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_files", type="integer", nullable=false)
     */
    private $countFiles;

    /**
     * @var string
     *
     * @ORM\Column(name="thumb", type="string", length=255, nullable=false)
     */
    private $thumb;


}

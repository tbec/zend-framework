<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcAdminConfig
 *
 * @ORM\Table(name="xc_admin_config")
 * @ORM\Entity
 */
class XcAdminConfig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=500, nullable=false)
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;


}

<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcBlocks
 *
 * @ORM\Table(name="xc_blocks")
 * @ORM\Entity
 */
class XcBlocks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_block", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idBlock;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_template", type="bigint", nullable=false)
     */
    private $idTemplate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_location", type="integer", nullable=false)
     */
    private $idLocation;

    /**
     * @var integer
     *
     * @ORM\Column(name="depth", type="integer", nullable=false)
     */
    private $depth;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=false)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=255, nullable=false)
     */
    private $class;

    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=false)
     */
    private $width;


}

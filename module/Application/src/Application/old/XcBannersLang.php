<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcBannersLang
 *
 * @ORM\Table(name="xc_banners_lang")
 * @ORM\Entity
 */
class XcBannersLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="bigint", nullable=false)
     */
    private $idLang;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_banner", type="bigint", nullable=false)
     */
    private $idBanner;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;


}

<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcProductsLang
 *
 * @ORM\Table(name="xc_products_lang")
 * @ORM\Entity
 */
class XcProductsLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="id_lang", type="boolean", nullable=false)
     */
    private $idLang;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="bigint", nullable=false)
     */
    private $idProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="vat", type="integer", nullable=false)
     */
    private $vat;

    /**
     * @var float
     *
     * @ORM\Column(name="price_brutto", type="decimal", nullable=false)
     */
    private $priceBrutto;

    /**
     * @var float
     *
     * @ORM\Column(name="price_netto", type="decimal", nullable=false)
     */
    private $priceNetto;

    /**
     * @var float
     *
     * @ORM\Column(name="price_promo", type="decimal", nullable=false)
     */
    private $pricePromo;

    /**
     * @var string
     *
     * @ORM\Column(name="cache", type="text", nullable=false)
     */
    private $cache;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=300, nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=300, nullable=false)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=300, nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=300, nullable=false)
     */
    private $tags;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;


}

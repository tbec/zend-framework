<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcProductsRelated
 *
 * @ORM\Table(name="xc_products_related")
 * @ORM\Entity
 */
class XcProductsRelated
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_produktu", type="bigint", nullable=false)
     */
    private $idProduktu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_produktu2", type="bigint", nullable=false)
     */
    private $idProduktu2;


}

<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcConfig
 *
 * @ORM\Table(name="xc_config")
 * @ORM\Entity
 */
class XcConfig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa_firmy", type="string", length=255, nullable=false)
     */
    private $nazwaFirmy;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=false)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="adres", type="string", length=255, nullable=false)
     */
    private $adres;

    /**
     * @var string
     *
     * @ORM\Column(name="kod_pocztowy", type="string", length=6, nullable=false)
     */
    private $kodPocztowy;

    /**
     * @var string
     *
     * @ORM\Column(name="miasto", type="string", length=255, nullable=false)
     */
    private $miasto;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefon", type="string", length=255, nullable=false)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics_id", type="string", length=255, nullable=false)
     */
    private $analyticsId;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics_email", type="string", length=255, nullable=false)
     */
    private $analyticsEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics_pass", type="string", length=255, nullable=false)
     */
    private $analyticsPass;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics_tracking", type="string", length=255, nullable=false)
     */
    private $analyticsTracking;

    /**
     * @var integer
     *
     * @ORM\Column(name="smtp", type="integer", nullable=false)
     */
    private $smtp;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_host", type="string", length=255, nullable=false)
     */
    private $smtpHost;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_login", type="string", length=255, nullable=false)
     */
    private $smtpLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="smtp_password", type="string", length=255, nullable=false)
     */
    private $smtpPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=255, nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=false)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=255, nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_cache", type="text", nullable=false)
     */
    private $profileCache;

    /**
     * @var string
     *
     * @ORM\Column(name="gallery", type="string", length=500, nullable=false)
     */
    private $gallery;

    /**
     * @var string
     *
     * @ORM\Column(name="modules", type="string", length=255, nullable=false)
     */
    private $modules;

    /**
     * @var integer
     *
     * @ORM\Column(name="wiadomosci", type="bigint", nullable=false)
     */
    private $wiadomosci;

    /**
     * @var integer
     *
     * @ORM\Column(name="liczba_odwiedzin", type="bigint", nullable=false)
     */
    private $liczbaOdwiedzin;

    /**
     * @var integer
     *
     * @ORM\Column(name="newsletter_count", type="integer", nullable=false)
     */
    private $newsletterCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="formularz_produktowy", type="integer", nullable=false)
     */
    private $formularzProduktowy;

    /**
     * @var string
     *
     * @ORM\Column(name="product", type="string", length=255, nullable=false)
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="listing", type="string", length=255, nullable=false)
     */
    private $listing;

    /**
     * @var string
     *
     * @ORM\Column(name="news", type="string", length=255, nullable=false)
     */
    private $news;

    /**
     * @var string
     *
     * @ORM\Column(name="boxes", type="string", length=255, nullable=false)
     */
    private $boxes;

    /**
     * @var string
     *
     * @ORM\Column(name="layout", type="string", length=255, nullable=false)
     */
    private $layout;

    /**
     * @var integer
     *
     * @ORM\Column(name="www_redirect", type="integer", nullable=false)
     */
    private $wwwRedirect;

    /**
     * @var string
     *
     * @ORM\Column(name="robots_txt", type="string", length=500, nullable=false)
     */
    private $robotsTxt;

    /**
     * @var string
     *
     * @ORM\Column(name="twoje_zaplecze", type="string", length=255, nullable=false)
     */
    private $twojeZaplecze;


}

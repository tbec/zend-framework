<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XcManufacturersLang
 *
 * @ORM\Table(name="xc_manufacturers_lang")
 * @ORM\Entity
 */
class XcManufacturersLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_producenta", type="bigint", nullable=false)
     */
    private $idProducenta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="id_lang", type="boolean", nullable=false)
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa", type="string", length=255, nullable=false)
     */
    private $nazwa;

    /**
     * @var string
     *
     * @ORM\Column(name="krotki_opis", type="string", length=255, nullable=false)
     */
    private $krotkiOpis;

    /**
     * @var string
     *
     * @ORM\Column(name="opis", type="text", nullable=false)
     */
    private $opis;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=300, nullable=false)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=300, nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=300, nullable=false)
     */
    private $metaTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;


}

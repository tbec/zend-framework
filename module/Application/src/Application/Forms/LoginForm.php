<?php
namespace Application\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

class LoginForm extends Form
{
    public function __construct($name = null)
		{
        // we want to ignore the name passed
        parent::__construct('user-login-form');
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'user-login-form');
		$this->setAttribute('class', 'user-login-form');
        $this->setAttribute('action', '/account/login');
		
	
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type'  => 'text',
				'id'=>'login',
				'placeholder'=>'Login or email'
            ),
            'options' => array(
                
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'password',
				'id'=>'password',
				'placeholder'=>'Password'
            ),
            'options' => array(
                
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Login',
                'id' => 'submitbutton',
				'class'=>'button big'
            ),
        ));
    }
}
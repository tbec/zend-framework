<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\FeedModel;
class IndexController extends ActionController
{
    public function indexAction()
    {
       $request = $this->getRequest();
       $response = $this->getResponse();
       
       echo $request->isXmlHttpRequest(); //check AJAX request
       echo $request->isPost(); //check Post
	   
       $this->view_data['breadcrumbs'] = 'Home &raquo; Pages';
	   
       return $this->_render();
    }
    
	public function sitemapAction(){
		
		$this->getResponse()->getHeaders()->addHeaders(array('Content-type' => 'text/xml')); 
		return new FeedModel(); 
	
	}
	
    public function ajaxAction(){
        
        #using json strategy
        return new JsonModel(array(
        'success'=>true
        )); 
        
        #simple json output
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode(array('success'=>1)));
        return $response;
    }
}

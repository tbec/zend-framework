<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionController
 *
 * @author Tomasz
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ActionController extends AbstractActionController{
    
    public $em = null;
    public $view_data = array();
	public $auth = null;
	
	public function _render($custom=''){
		/*
		$this->view_data = array_merge($this->view_data,array(
                    'items' => $this->ZendCart()->cart(),
                    'total_items' => $this->ZendCart()->total_items(),
                    'total' => $this->ZendCart()->total()
                 ));
		 */
		 if($this->view_data) foreach($this->view_data as $k=>$v)
		 $this->layout()->setVariable($k,$v);
         
		 $auth = $this->getAuth();
		 if ($auth->hasIdentity())  $this->view_data['user'] = $auth->getIdentity();
		 
		 
		 $view = new ViewModel($this->view_data);
		 if($custom) $view->setTemplate($custom);

		 return $view; 
	}
	
	public function getAuth()
	{
	 return $this->getServiceLocator()->get('doctrine.authenticationservice.orm_default');
	}
	
	
	public function getBlocks($block_ids)
	{
	
	}
	
	
	public function getPages($page_ids)
	{
	
	}
	
	public function _json($data)
	{
		 #simple json output
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($data));
        return $response;
	}
}

?>

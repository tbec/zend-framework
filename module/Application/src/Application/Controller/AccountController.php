<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Form\Annotation\AnnotationBuilder;
use Application\Forms\LoginForm;
class AccountController extends ActionController
{	
	public function getSessionStorage()
    {
        if (! $this->storage) {
             $this->getServiceLocator()->get('doctrine.authenticationservice.orm_default');
        }
         
        return $this->storage;
    }

    public function indexAction()
    {  
	
	   $auth = $this->getAuth();
	   if (!$auth->hasIdentity()) return $this->redirect()->toUrl('/account/login');
	   return $this->_render();
	}
	
	
	public function authenticate($login,$password)
	{
		$auth = $this->getAuth();
		
	   
        
        $auth->getAdapter()->setIdentityValue($login);
        $auth->getAdapter()->setCredentialValue(sha1($password));
        return $auth->authenticate();  
	}
	
	
	public function loginAction()
	{  
		$request = $this->getRequest();
		$form = new LoginForm();
 
		if($request->isPost()) {			
			$login = $request->getPost('login');
			$password = $request->getPost('password');
			$data = $errors = array();
			
			
			$objectManager = $this
			->getServiceLocator()
			->get('Doctrine\ORM\EntityManager');
			$objectManager->getRepository('Application\Entity\XcAccounts');
			$account = new \Application\Entity\XcAccounts();
			
            $form->setInputFilter($account->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                
            
			
			$result = $this->authenticate($login,$password);
			$code = (int)$result->getCode();
			
			if($code>0) list($type,$title,$msg,$data,$errors) = array(1,'Success','You have been successfully logged in',$data,$errors);
			else list($type,$title,$msg,$data,$errors) = array(0,'Error','Incorrect login',$data,$errors);
			
			$result = array('type'=>$type,'title'=>$title,'msg'=>$msg);
			
			
			}
			else
			{
				foreach($form->getMessages() as $k=>$m)
				{
				$errors[$k] = array_shift($m);
				}
			
				list($type,$title,$msg,$data,$errors) = array(0,'Error','Incorrect login','',$errors);	
			}
			
			$result = array('type'=>$type,'title'=>$title,'msg'=>$msg,'data'=>$data,'errors'=>$errors,'redirect'=>'/account');
			if($msg) $this->view_data['msg'] = $result;
			
			if($request->isXmlHttpRequest()) return $this->_json($result);
			else $this->view_data['msg'] = $result;
		}
		
		
		$this->view_data['form'] = $form;

		
		$auth = $this->getAuth();
		if ($auth->hasIdentity()) return $this->redirect()->toUrl('/account');
		
		return $this->_render();
	}
	
	
	public function register($login,$password,$email){
		
		$errors = array();
		$data = array();
		$v = new \Zend\Validator\EmailAddress();
		if(!$v->isValid($email)) $errors['email'] = $v->getMessage();
		if(empty($errors))
		{
			$objectManager = $this
			->getServiceLocator()
			->get('Doctrine\ORM\EntityManager');
			$objectManager->getRepository('Application\Entity\XcAccounts');
			$account = new \Application\Entity\XcAccounts();
			$account->setLogin($login);
			$account->setPassword(sha1($password));
			$account->setFirstname($login);
			$account->setLastname($login);
			$account->setHash(md5($login));
			$account->setType(1);
			$account->setLoggedin(0);
			$account->setActive(1);
			$account->setEmail($email);
			
			
			
			$objectManager->persist($account);
			$objectManager->flush();
			if($account->getIdAccount()) 
			{
			$data['account_id'] = $account->getIdAccount();
			$code = 1; 
			}
			else 
			{
			$code = 0; 
			}
			
		}
		else
		{
			$code = 0;
		}
		
		return array('code'=>$code,'errors'=>$errors,'data'=>$data);
	}
	
	
	public function registerAction(){
		
		$request = $this->getRequest();
		
		if($request->isPost()) {
			$login = $request->getPost('login');
			$email = $request->getPost('email');
			$password = $request->getPost('password');
			$rpassword = $request->getPost('rpassword');
			$result = $this->register($login,$password,$email);
			
			if($result['code']>0) list($type,$title,$msg,$data,$errors) = array(1,'Success','Your account has been created',$result['data'],$result['errors']);
			else list($type,$title,$msg,$data,$errors) = array(0,'Error','Please correct errors and try again',$result['data'],$result['errors']);
			
			$result = array('type'=>$type,'title'=>$title,'msg'=>$msg,'data'=>$data,'errors'=>$errors);
			if($request->isXmlHttpRequest()) return $this->_json($result);
			else $this->view_data['msg'] = $result;
		}
		
		
		$this->headScript()
		->appendFile(BASE_PATH."/js/jquery.form.js", "text/javascript")
		->appendFile(BASE_PATH."/js/api/user.js", "text/javascript");
		
		return $this->_render('application/account/login.tpl');
	}
	
	public function logoutAction(){
		$auth = $this->getAuth();
		if ($auth->hasIdentity()) $auth->clearIdentity();
		return $this->redirect()->toUrl('/account/login');
	}
}
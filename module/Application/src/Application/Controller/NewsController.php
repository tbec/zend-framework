<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Controller\ActionController;
use Zend\View\Model\ViewModel;

class NewsController extends ActionController
{
    public function indexAction()
    {  
		return new ViewModel(array(
			
		));
    }
	
	public function detailsAction()
    {  
		return new ViewModel(array(
			
		));
    }
}
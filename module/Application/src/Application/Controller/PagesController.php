<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Controller\ActionController;
use Zend\View\Model\ViewModel;

class PagesController extends ActionController
{
    public function indexAction()
    {  

		return $this->_render();
    }
	
	public function detailsAction()
    {  

		$id =  $this->params()->fromQuery('id');
	
		if(!is_numeric($id)) return $this->redirect()->toUrl('error/404');
		
		 $objectManager = $this
        ->getServiceLocator()
        ->get('Doctrine\ORM\EntityManager');

		$res = $objectManager->getRepository('Application\Entity\XcPages')->find($id);
		
		$page = array(
		'title'=>$res->getTitle(),
		'content'=>$res->getContent()
		);
		
	
		
		$this->view_data = $page;
		return $this->_render();
	}
}
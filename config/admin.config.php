<?php
return array(
    'modules' => array(
        'DoctrineModule',
        'DoctrineORMModule',
        'DoctrineMigrationsModule',
        'SmartyModule',
        'Admin',
        'WebinoImageThumb'
        #'ZendDeveloperTools'

    ),
    'module_listener_options' => array(
        'config_glob_paths' => array(
             'config/autoload/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            './module',
            './vendor',
        ),
		/*
        'config_cache_enabled' => true,
        'config_cache_key' => "2245023265ae4cf87d02c8b6ba991139",
        'module_map_cache_enabled' => true,
        'module_map_cache_key' => "496fe9daf9baed5ab03314f04518b928",
        'cache_dir' => "./data/cache/modulecache",
		*/
	),
);
